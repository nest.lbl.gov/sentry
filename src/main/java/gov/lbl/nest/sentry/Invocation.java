package gov.lbl.nest.sentry;

import gov.lbl.nest.common.configure.InitializingException;

/**
 * This interface us used to invoke execution of an action.
 * 
 * @author patton
 */
public interface Invocation {

    /**
     * Executes using the supplied {@link RealizedAction} instance.
     * 
     * @param action
     *            the {@link RealizedAction} instance with which this method was
     *            invoked.
     * @param clazz
     *            the return type of the execution.
     * 
     * @return the result of executing this object.
     * 
     * @throws ExecutionException
     *             when the execution has not completed successfully.
     * @throws InitializingException
     *             when the application is still initializing.
     */
    <T> T execute(RealizedAction action,
                  Class<T> clazz) throws ExecutionException,
                                  InitializingException;

    /**
     * Returns the description of this invocation, if any.
     * 
     * @return the description of this invocation, if any.
     */
    String getDescription();
}
