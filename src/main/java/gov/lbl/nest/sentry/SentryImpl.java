package gov.lbl.nest.sentry;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import jakarta.annotation.Resource;
import jakarta.ejb.AccessTimeout;
import jakarta.ejb.Lock;
import jakarta.ejb.LockType;
import jakarta.ejb.SessionContext;
import jakarta.ejb.Singleton;
import jakarta.ejb.TransactionAttribute;
import jakarta.ejb.TransactionAttributeType;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.common.configure.InitializingException;
import gov.lbl.nest.common.monitor.Instrumentation;
import gov.lbl.nest.common.suspension.Suspendable;
import gov.lbl.nest.sentry.config.Action;
import gov.lbl.nest.sentry.config.ApplicationSuspension;
import gov.lbl.nest.sentry.config.Configuration;

/**
 * This class watches for changes.
 * 
 * @author patton
 */
@Singleton
@AccessTimeout(value = 10,
               unit = TimeUnit.MINUTES)
@Lock(LockType.READ)
public class SentryImpl implements
                        Sentry {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(SentryImpl.class);

    // private static member data

    // private instance member data

    /**
     * The {@link List} to return when the return list does not exist.
     */
    private final List<String> EMPTY_LIST = new ArrayList<String>();

    /**
     * The collection of {@link Realization} instances.
     */
    private final Map<String, Realization> REALIZATIONS = new HashMap<String, Realization>();

    /**
     * The collection of list that define the contents of each task.
     */
    private final Map<String, List<String>> TASKS = new HashMap<String, List<String>>();

    /**
     * The {@link Configuration} instance containing the configuration settings for
     * Sentry.
     */
    @Inject
    private Configuration configuration;

    /**
     * The {@link Context} instance suppling the execution context to the created
     * {@link Realization} instances.
     */
    @Inject
    private Context context;

    /**
     * The {@link EntityManager} instance used by this object.
     */
    @Inject
    @SentryDB
    private EntityManager entityManager;

    /**
     * The {@link InvocationExecuter} instance used by this object.
     */
    @Inject
    private InvocationExecuter executer;

    /**
     * True if the {@link Instrumentation} system has been initialized for this
     * object.
     */
    private boolean instrumentationInitialized = false;

    /**
     * The {@link Instrumentation} instance, if any, used by this object.
     */
    private Instrumentation instrumentation;

    /**
     * This object's {@link SessionContext} instance used to trigger new
     * transactions.
     */
    @Resource
    SessionContext sessionContext;

    /**
     * The {@link ApplicationSuspension} instance containing the configuration
     * settings for Sentry.
     */
    @Inject
    @SentrySuspension
    private Suspendable suspended;

    /**
     * THe list of tasks whose warning has been issued once.
     */
    private final List<String> warned = new ArrayList<String>();

    // constructors

    // instance member method (alphabetic)

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void execute(final String action) throws ExecutionException,
                                             TimeoutException,
                                             InitializingException {
        initialize();
        executeAction(action,
                      new Invocation() {

                          @Override
                          public <T> T execute(RealizedAction realizedAction,
                                               Class<T> clazz) throws ExecutionException,
                                                               InitializingException {
                              realizedAction.execute();
                              return null;
                          }

                          @Override
                          public String getDescription() {
                              return "Running action \'" + action
                                     + "\'";
                          }
                      },
                      Object.class);
    }

    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public <T> T executeAction(final String action,
                               final Invocation invocation,
                               final Class<T> clazz) throws ExecutionException,
                                                     TimeoutException,
                                                     InitializingException {
        initialize();
        final Realization realization = REALIZATIONS.get(action);
        if (null == realization) {
            throw new IllegalArgumentException("No such action as \"" + action
                                               + "\"");
        }
        if (realization.isSuspended()) {
            if (null == invocation.getDescription()) {
                LOG.info("Action \"" + action
                         + "\" suspended so will not be executed");
            } else {
                LOG.info("Action \"" + action
                         + "\" suspended invocation of \""
                         + invocation.getDescription()
                         + "\" will be skipped");
            }
            return null;
        }
        LOG.info("Executing the action \"" + action
                 + "\"");
        final UUID uuid = UUID.randomUUID();
        final Map<String, String> attributes = new HashMap<String, String>(1);
        attributes.put("action",
                       action);
        Instrumentation instrumentationToUse = getInstrumentation();
        if (null != instrumentationToUse) {
            instrumentationToUse.start(uuid,
                                       attributes);
        }
        try {
            final T result = realization.execute(invocation,
                                                 executer,
                                                 clazz);
            if (null != instrumentationToUse) {
                instrumentationToUse.end(uuid,
                                         attributes);
            }
            if (null == invocation.getDescription()) {
                LOG.info("Completed \"" + action
                         + "\"");
            } else {
                LOG.info("Completed invocation of \"" + invocation.getDescription()
                         + "\" for \""
                         + action
                         + "\"");
            }
            return result;
        } catch (ExecutionException e) {
            if (null != instrumentationToUse) {
                instrumentationToUse.error(-1,
                                           uuid,
                                           attributes);
            }

            final Throwable cause;
            if (null == e.getCause()) {
                cause = e;
            } else {
                cause = e.getCause();
            }
            final String message;
            if (null == cause.getMessage()) {
                message = "";
            } else {
                message = " with message \"" + cause.getMessage()
                          + "\"";
            }
            final Class<?> causeClazz = cause.getClass();
            LOG.error("Failed to complete \"" + action
                      + "\" due to \""
                      + causeClazz.getCanonicalName()
                      + "\" being thrown"
                      + message);
            throw e;
        }
    }

    @Override
    public void executeTask(String task) throws InitializingException {
        if (isSuspended()) {
            return;
        }
        final Collection<String> actions = getActions(task);
        if (0 == actions.size()) {
            if (!warned.contains(task)) {
                LOG.warn("Task \"" + task
                         + "\" contains no actions, so will not be automatically executed");
                warned.add(task);
            }
            return;
        }
        final String plural;
        if (1 == actions.size()) {
            plural = "";
        } else {
            plural = "s";
        }
        if ("rapid".equals(task)) {
            // Does nothing as would be too noisy.
        } else {
            final StringBuilder sb = new StringBuilder("Automatically executing the \"" + task
                                                       + "\" task which is made up of the following action"
                                                       + plural
                                                       + ": ");
            String conjunction = "";
            for (String action : actions) {
                sb.append(conjunction + "\""
                          + action
                          + "\"");
                conjunction = ", ";
            }
            LOG.info(sb.toString());
        }
        int failureCount = 0;
        final UUID uuid = UUID.randomUUID();
        final Map<String, String> attributes = new HashMap<String, String>(1);
        attributes.put("task",
                       task);
        Instrumentation instrumentationToUse = getInstrumentation();
        if (null != instrumentationToUse) {
            instrumentationToUse.start(uuid,
                                       attributes);
        }
        for (String action : actions) {
            try {
                // By executing each action via the business object they execute
                // in separate transactions.
                final Sentry self = sessionContext.getBusinessObject(Sentry.class);
                self.execute(action);
            } catch (TimeoutException e) {
                LOG.warn("Skipped \"" + action
                         + "\" as already executing");
            } catch (Exception e) {
                failureCount = +1;
                e.printStackTrace();
            }
        }
        if (0 == failureCount) {
            if (null != instrumentationToUse) {
                instrumentationToUse.end(uuid,
                                         attributes);
            }
        } else {
            if (null != instrumentationToUse) {
                instrumentationToUse.error(failureCount,
                                           uuid,
                                           attributes);
            }
        }
    }

    @Override
    public Collection<String> getActions() throws InitializingException {
        initialize();
        return REALIZATIONS.keySet();
    }

    @Override
    public Collection<String> getActions(String task) throws InitializingException {
        initialize();
        final List<String> result = TASKS.get(task);
        if (null == result) {
            return EMPTY_LIST;
        }
        return Collections.unmodifiableCollection(result);
    }

    /**
     * Returns the collections of {@link Action} instances that use the specified
     * {@link Class}.
     * 
     * @param clazz
     *            the class whose {@link Action} instances should be returned.
     * 
     * @return the collections of {@link Action} instances that use the specified
     *         {@link Class}. execute.
     */
    public Collection<Action> getActions(Class<?> clazz) {
        final String target = clazz.getCanonicalName();
        final List<Action> result = new ArrayList<Action>();
        for (Action action : configuration.getActions()) {
            if (action.getActionClass()
                      .equals(target)) {
                result.add(action);
            }
        }
        return result;
    }

    @Override
    public Configuration getConfiguration() {
        return configuration;
    }

    /**
     * Returns the {@link EntityManager} instance, if any, used by the specified
     * action.
     * 
     * @param action
     *            the action whose {@link EntityManager} instance should be
     *            returned.
     * 
     * @return the {@link EntityManager} instance, if any, used by the specified
     *         action.
     * 
     * @throws InitializingException
     *             when the application is still initializing.
     */
    public EntityManager getEntityManager(String action) throws InitializingException {
        initialize();
        return entityManager;
    }

    @Override
    public String getName() {
        return configuration.getName();
    }

    private Instrumentation getInstrumentation() {
        if (instrumentationInitialized) {
            return instrumentation;
        }
        instrumentation = Instrumentation.getInstrumentation(getClass().getSimpleName());
        return instrumentation;
    }

    /**
     * Initializes the application when it is first used.
     * 
     * @throws InitializingException
     *             when the application is still initializing.
     */
    private void initialize() throws InitializingException {
        synchronized (REALIZATIONS) {
            if (REALIZATIONS.isEmpty()) {
                final List<Action> actionList = configuration.getActions();
                if (null != actionList && !actionList.isEmpty()) {
                    for (Action action : actionList) {
                        final String name = action.getName();
                        try {
                            final Realization realization = new Realization(action,
                                                                            suspended.isSuspended(),
                                                                            context,
                                                                            this);
                            REALIZATIONS.put(name,
                                             realization);
                            final List<String> actionTasks = action.getTasks();
                            if (null != actionTasks) {
                                for (String actionTask : actionTasks) {
                                    List<String> taskContent = TASKS.get(actionTask);
                                    if (null == taskContent) {
                                        taskContent = new ArrayList<String>();
                                        TASKS.put(actionTask,
                                                  taskContent);
                                    }
                                    taskContent.add(name);
                                }
                            }
                            LOG.info("Instantiated action \"" + name
                                     + "\"");
                        } catch (ClassNotFoundException e) {
                            throw new IllegalArgumentException("No such class as \"" + action.getActionClass()
                                                               + "\"",
                                                               e);
                        } catch (InstantiationException e) {
                            throw new IllegalArgumentException("Can not instantiate class \"" + action.getActionClass()
                                                               + "\"",
                                                               e);
                        } catch (InvocationTargetException e) {
                            throw new IllegalArgumentException("Constructor of class \"" + action.getActionClass()
                                                               + "\" has thrown an expection",
                                                               e);
                        }
                    }
                }
            }
        }
    }

    @Override
    public boolean isSuspended() throws InitializingException {
        initialize();
        return suspended.isSuspended();
    }

    @Override
    public boolean isSuspended(String task) throws InitializingException {
        initialize();
        final Collection<String> actions = getActions(task);
        for (String action : actions) {
            if (!isSuspendedAction(action)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean isSuspendedAction(String action) throws InitializingException {
        initialize();
        final Realization realization = REALIZATIONS.get(action);
        if (null == realization) {
            LOG.warn("\"" + action
                     + "\" is not suspendable so its state has not changed");
        }
        return realization.isSuspended();
    }

    @Override
    @Lock(LockType.WRITE)
    public boolean setSuspended(boolean suspended) throws InitializingException {
        initialize();
        boolean result = (isSuspended() != suspended);
        this.suspended.setSuspended(suspended);
        for (Realization realization : REALIZATIONS.values()) {
            realization.setSuspended(suspended);
        }

        final boolean current = isSuspended();
        if (suspended == current) {
            if (suspended) {
                LOG.info("Application has been suspended");
            } else {
                LOG.info("Application has been resumed");
            }
        } else {
            if (suspended) {
                LOG.info("Application failed to be suspended");
            } else {
                LOG.info("Application failed to be resumed");
            }
        }
        return result;
    }

    @Override
    @Lock(LockType.WRITE)
    public boolean setSuspended(final String task,
                                final boolean suspended) throws InitializingException {
        initialize();
        boolean result = (isSuspended(task) != suspended);
        final Collection<String> actions = getActions(task);
        for (String action : actions) {
            setSuspendedAction(action,
                               suspended);
        }

        final boolean current = isSuspended(task);
        if (suspended == current) {
            if (suspended) {
                LOG.info("Task \"" + task
                         + "\" has been suspended");
            } else {
                LOG.info("Task \"" + task
                         + "\" has been resumed");
            }
        } else {
            if (suspended) {
                LOG.info("Task \"" + task
                         + "\" failed to be suspended");
            } else {
                LOG.info("Task \"" + task
                         + "\"  failed to be resumed");
            }
        }
        return result;
    }

    @Override
    @Lock(LockType.WRITE)
    public boolean setSuspendedAction(final String action,
                                      final boolean suspended) throws InitializingException {
        initialize();
        final Realization realization = REALIZATIONS.get(action);
        if (null == realization) {
            LOG.warn("\"" + action
                     + "\" is not suspendable so its state has not changed");
        }
        boolean result = (isSuspendedAction(action) != suspended);
        realization.setSuspended(suspended);

        final boolean current = isSuspendedAction(action);
        if (suspended == current) {
            if (suspended) {
                LOG.info("Action \"" + action
                         + "\" has been suspended");
            } else {
                LOG.info("Action \"" + action
                         + "\" has been resumed");
            }
        } else {
            if (suspended) {
                LOG.info("Action \"" + action
                         + "\" failed to be suspended");
            } else {
                LOG.info("Action \"" + action
                         + "\"  failed to be resumed");
            }
        }
        return result;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}