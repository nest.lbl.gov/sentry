package gov.lbl.nest.sentry;

import java.util.Collection;
import java.util.concurrent.TimeoutException;

import gov.lbl.nest.common.configure.InitializingException;
import gov.lbl.nest.common.suspension.Suspendable;
import gov.lbl.nest.sentry.config.Action;
import gov.lbl.nest.sentry.config.Configuration;

/**
 * This interface defines what the Sentry application can do.
 * 
 * @author patton
 */
public interface Sentry extends
                        Suspendable {

    /**
     * The version of the specification that this library implements.
     */
    static final String SPECIFICATION = "2.0";

    /**
     * Executes the named {@link Action}.
     * 
     * @param action
     *            the name of the {@link Action} instance to execute.
     * 
     * @throws ExecutionException
     *             when the execution has not completed successfully.
     * @throws TimeoutException
     *             when the action is already being executed.
     * @throws InitializingException
     *             when the application is still initializing.
     */
    void execute(final String action) throws ExecutionException,
                                      TimeoutException,
                                      InitializingException;

    /**
     * Executes the named {@link Action}.
     * 
     * @param action
     *            the name of the {@link Action} instance to execute.
     * @param invocation
     *            the {@link Invocation} instance to execute.
     * @param clazz
     *            the returned class of the invocation.
     * 
     * @return the result of executing the specified {@link Invocation} instance.
     * 
     * @throws ExecutionException
     *             when the execution has not completed successfully.
     * @throws TimeoutException
     *             when the action is already being executed.
     * @throws InitializingException
     *             when the action can not be initialized.
     * 
     */
    public <T> T executeAction(final String action,
                               final Invocation invocation,
                               final Class<T> clazz) throws ExecutionException,
                                                     TimeoutException,
                                                     InitializingException;

    /**
     * Executes the specified task
     * 
     * @param task
     *            the name of the task to execute.
     * 
     * @throws InitializingException
     *             when the application is still initializing.
     */
    public void executeTask(String task) throws InitializingException;

    /**
     * Returns the collections of {@link Action} names that this instance can
     * execute.
     * 
     * @return the collections of {@link Action} names that this instance can
     *         execute.
     * 
     * @throws InitializingException
     *             when the application is still initializing.
     */
    Collection<String> getActions() throws InitializingException;

    /**
     * Returns the collections of {@link Action} names that belong to the specified
     * task.
     * 
     * @param task
     *            the name of the task whose actions should be returned.
     * 
     * @return the collections of {@link Action} names that belong to the specified
     *         task.
     * 
     * @throws InitializingException
     *             when the application is still initializing.
     */
    Collection<String> getActions(String task) throws InitializingException;

    /**
     * Returns the {@link Configuration} instance used by this application.
     * 
     * @return the {@link Configuration} instance used by this application.
     */
    Configuration getConfiguration();

    /**
     * Returns true when the supplied task has been suspended.
     * 
     * @param task
     *            the name of the task to be tested.
     * 
     * @return true when the supplied task has been suspended.
     * 
     * @throws InitializingException
     *             when the application is still initializing.
     */
    boolean isSuspended(final String task) throws InitializingException;

    /**
     * Returns true when the supplied action has been suspended.
     * 
     * @param action
     *            the name of the action to be tested.
     * 
     * @return true when the supplied action has been suspended.
     * 
     * @throws InitializingException
     *             when the application is still initializing.
     */
    boolean isSuspendedAction(final String action) throws InitializingException;

    /**
     * Sets whether a task of this object should be suspended or not.
     * 
     * @param task
     *            the name of the task whose whose state should be set.
     * @param suspended
     *            true is an task of this object should be suspended
     * 
     * @return True if the state of the element being controlled by this Object has
     *         changed.
     * 
     * @throws InitializingException
     *             when the application is still initializing.
     */
    public boolean setSuspended(final String task,
                                final boolean suspended) throws InitializingException;

    /**
     * Sets whether an action of this object should be suspended or not.
     * 
     * @param action
     *            the name of the action whose whose state should be set.
     * @param suspended
     *            true if an action of this object should be suspended.
     * 
     * @return True if the state of the element being controlled by this Object has
     *         changed.
     * 
     * @throws InitializingException
     *             when the application is still initializing.
     */
    public boolean setSuspendedAction(final String action,
                                      final boolean suspended) throws InitializingException;

}