package gov.lbl.nest.sentry.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import jakarta.enterprise.inject.Vetoed;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.Unmarshaller;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlElementWrapper;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlTransient;
import jakarta.xml.bind.annotation.XmlType;

import gov.lbl.nest.common.configure.ConfigurationDirectory;
import gov.lbl.nest.common.configure.CustomConfigPath;

/**
 * This class contains the configuration for the Sentry application.
 * 
 * The {@link #load()} method of this class loads the configuration for a file
 * that is found by the following rules.
 * </p>
 * <p/>
 * <ol>
 * <p/>
 * <li>If the system property <code>gov.lbl.nest.sentry.Configuration</code> is
 * defined then it is taken to be the path, either absolute or from the
 * applications working directory, to the XML file contains the setting.</li>
 * <p/>
 * <li>If a provider class has been installed in a file that is visible to the
 * system class loader, and that file contains a provider-configuration file
 * named <code>gov.lbl.nest.sentry.Configuration</code> in the resource
 * directory <code>META-INF/config</code>, then the first non-empty line
 * specified in that file is taken to be the file name.</li>
 * <p/>
 * <li>Finally, if no provider has been specified by any of the above means then
 * the system-default name, <code>sentry/sentry.xml</code> is used.</li>
 * <p/>
 * </ol>
 * <p>
 * 
 * @author patton
 */
@XmlRootElement(name = "sentry_config")
@XmlType(propOrder = { "name",
                       "actions",
                       "mailProperties" })
@Vetoed
public class Configuration implements
                           ConfigurationDirectory {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The default path to the XML file containing the settings for this object.
     */
    private static final String SPADE_CONFIG_DEFAULT_PATH = "sentry/sentry.xml";

    /**
     * The name of the resource that may contain the path to an alternate
     * configuration file for Sentry.
     */
    private static final String SENTRY_CONFIG_RESOURCE = "sentry_config";

    /**
     * The {@link CustomConfigPath} instance that may contain an alternate path
     * to the configuration file.
     */
    private static CustomConfigPath SENTRY_CONFIG_CUSTOM_PATH = new CustomConfigPath(Configuration.class,
                                                                                     SENTRY_CONFIG_RESOURCE);

    /**
     * The name of the resource that may contain the path to an alternate
     * <q>suspend<\q> file for SPADE.
     */
    private static final String SUSPEND_FILE_RESOURCE = "suspend_file";

    /**
     * The default path to the file whose existence means the Application is
     * initially suspended. (relative to the configuration directory).
     */
    private static final String SUSPEND_FILE_DEFAULT_PATH = "sentry.suspended";

    /**
     * True if the {@link #configFileName} value has been loaded.
     */
    private static CustomConfigPath SUSPEND_FILE_CONFIG_PATH = new CustomConfigPath(ApplicationSuspension.class,
                                                                                    SUSPEND_FILE_RESOURCE);

    /**
     * The name of the system property containing the user's home directory.
     */
    private static final String USER_HOME_PROPERTY = "user.home";

    /**
     * The {@link Marshaller} property to set formatting.
     */
    private static final String XML_FORMAT_PROPERTY = "jaxb.formatted.output";

    // private static member data

    // private instance member data

    /**
     * The collections of Action instances that this instance should execute.
     */
    private List<Action> actions;

    /**
     * The file contains the setting for this object.
     */
    private File file;

    /**
     * The path to the file containing this application's mail properties.
     */
    private String mailProperties;

    /**
     * The name with which to refer to this instance of the application.
     */
    private String name;

    // constructors

    // instance member method (alphabetic)

    /**
     * Assigns the values in the supplied object to this instance.
     * 
     * @param rhs
     *            the object whose values should be assigned to this object.
     * 
     * @return the current version of this object.
     */
    public Configuration assign(Configuration rhs) {
        if (this == rhs) {
            return this;
        }
        setActions(rhs.getActions());
        file = rhs.file;
        return this;
    }

    /**
     * Returns the collections of {@link Action} instances that this instance
     * should execute.
     * 
     * @return the collections of {@link Action} instances that this instance
     *         should execute.
     */
    @XmlElement(name = "action")
    @XmlElementWrapper(name = "actions")
    public List<Action> getActions() {
        return actions;
    }

    /**
     * Returns the directory in which these settings are kept.
     * 
     * @return the directory in which these settings are kept.
     */
    @XmlTransient
    public File getDirectory() {
        return file.getParentFile();
    }

    /**
     * Returns the file in which these settings are kept.
     * 
     * @return the file in which these settings are kept.
     */
    @XmlTransient
    public File getFile() {
        return file;
    }

    /**
     * Returns the path to the file containing this application's mail
     * properties.
     * 
     * @return the path to the file containing this application's mail
     *         properties.
     */
    @XmlElement(name = "mail_properties")
    public String getMailProperties() {
        return mailProperties;
    }

    /**
     * Returns the name with which to refer to this instance of the application.
     * 
     * @return the name with which to refer to this instance of the application.
     */
    @XmlElement
    public String getName() {
        return name;
    }

    /**
     * Reloads this object from its file.
     * 
     * @return the new version of this object.
     * 
     * @throws JAXBException
     *             when the XML file can not be successful parsed.
     */
    public Configuration reload() throws JAXBException {
        if (null == file) {
            return this;
        }
        try {
            return assign(Configuration.load(new FileInputStream(file)));
        } catch (FileNotFoundException e) {
            return this;
        }
    }

    /**
     * Saves the current content to its file.
     * 
     * @return the current version of this object.
     * 
     * @throws JAXBException
     *             when the saving can not be saved.
     */
    public Configuration save() throws JAXBException {
        JAXBContext content = JAXBContext.newInstance(this.getClass());
        Marshaller marshaller = content.createMarshaller();
        marshaller.setProperty(XML_FORMAT_PROPERTY,
                               Boolean.TRUE);
        marshaller.marshal(this,
                           file);
        return this;
    }

    /**
     * Sets the collections of {@link Action} instances that this instance
     * should execute.
     * 
     * @param actions
     *            the collections of Action instances that this instance should
     *            execute.
     */
    protected void setActions(List<Action> actions) {
        /*
         * The following FAILED on TomEE 1.7.0
         */
        // this.actions = new ArrayList<Action>(actions);
        this.actions = actions;
    }

    /**
     * Sets the path to the file containing this application's mail properties.
     * 
     * @param file
     *            the path to the file containing this application's mail
     *            properties.
     */
    protected void setMailProperties(String file) {
        this.mailProperties = file;
    }

    /**
     * Sets the name with which to refer to this instance of the application.
     * 
     * @param name
     *            the name with which to refer to this instance of the
     *            application.
     */
    protected void setName(String name) {
        this.name = name;
    }

    // static member methods (alphabetic)

    /**
     * Generate a new {@link Configuration} instance and save it in the
     * specified file.
     * 
     * @param file
     *            the {@link File} into which to store the generated
     *            configuration.
     * 
     * @return the generated {@link Configuration} instance.
     */
    private static Configuration generate(final File file) {
        final Configuration result = new Configuration();
        final List<Action> actions = new ArrayList<Action>();
        result.setActions(actions);
        result.file = file;
        return result;
    }

    /**
     * Returns the {@link File} that is used to state whether the Application
     * has been suspended or not.
     * 
     * @param directory
     *            the {@link ConfigurationDirectory} for this application.
     * 
     * @return the {@link File} that is used to state whether the Application
     *         has been suspended or not.
     */
    public static File getSuspendFile(ConfigurationDirectory directory) {
        final Path filePath;
        final Path configPath = SUSPEND_FILE_CONFIG_PATH.getConfigPath();
        if (null == configPath) {
            final File configDir = directory.getDirectory();
            filePath = (new File(configDir,
                                 SUSPEND_FILE_DEFAULT_PATH).getAbsoluteFile()).toPath();
        } else {
            filePath = configPath;
        }
        return filePath.toFile();
    }

    /**
     * Returns a new instance of a {@link Configuration}.
     * 
     * @return a new instance of a {@link Configuration}.
     * 
     * @throws JAXBException
     *             when the setting file can not be read.
     */
    public static Configuration load() throws JAXBException {

        final Path filePath;
        final Path configPath = SENTRY_CONFIG_CUSTOM_PATH.getConfigPath();
        if (null == configPath) {
            final String home = System.getProperty(USER_HOME_PROPERTY);
            filePath = (new File(home,
                                 SPADE_CONFIG_DEFAULT_PATH).getAbsoluteFile()).toPath();
        } else {
            filePath = configPath;
        }
        final File configFile = filePath.toFile();
        if (configFile.exists()) {
            try {
                final Configuration result = load(new FileInputStream(configFile));
                result.file = configFile;
                return result;
            } catch (FileNotFoundException e) {
                // Do nothing and proceed as if file did not exist!
            }
        }
        Configuration generated;
        try {
            generated = generate(configFile.getCanonicalFile());
            generated.save();
            return generated;
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Attempts to read in a {@link Configuration} instance from an XML file.
     * 
     * @param stream
     *            the {@link File} containing the XML.
     * 
     * @return the {@link Configuration} instance read from the file.
     * 
     * @throws JAXBException
     *             when the setting file can not be read.
     */
    public static Configuration load(final InputStream stream) throws JAXBException {
        JAXBContext content = JAXBContext.newInstance(Configuration.class);
        Unmarshaller unmarshaller = content.createUnmarshaller();
        final Configuration result = (Configuration) unmarshaller.unmarshal(stream);
        return result;
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
