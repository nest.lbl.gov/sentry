package gov.lbl.nest.sentry.config;

import java.util.ArrayList;
import java.util.List;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlType;

import gov.lbl.nest.common.configure.InitParam;

/**
 * This class describes an Action the the Sentry application should execute.
 * 
 * @author patton
 */
@XmlType(propOrder = { "name",
                       "tasks",
                       "actionClass",
                       "parameters" })
public class Action {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The canonical name of the class used to executes the action.
     */
    private String actionClass;

    /**
     * The name by which to refer to this object.
     */
    private String name;

    /**
     * The initialization parameters, is any, for this action.
     */
    private List<InitParam> parameters;

    /**
     * The collection of tasks to which the action belongs.
     */
    private List<String> tasks;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected Action() {
    }

    /**
     * Creates an instance of this class.
     * 
     * @param parameters
     *            the initialization parameters, is any, for this action.
     */
    public Action(List<InitParam> parameters) {
        setParameters(parameters);
    }

    // instance member method (alphabetic)

    /**
     * Returns the canonical name of the class used to executes the action.
     * 
     * @return the canonical name of the class used to executes the action.
     */
    @XmlElement(name = "class",
                required = true)
    public String getActionClass() {
        return actionClass;
    }

    /**
     * Returns the name by which to refer to this object.
     * 
     * @return the name by which to refer to this object.
     */
    @XmlElement(required = true)
    public String getName() {
        return name;
    }

    /**
     * Returns the value of the named parameter is specified, otherwise
     * <code>null</code>. If the named parameter is a list, then the value of
     * the first element in the list is returned.
     * 
     * @param paramName
     *            the name of the parameter to return.
     * 
     * @return the value of the named parameter is specified, otherwise
     *         <code>null</code>.
     */
    public String getParameter(final String paramName) {
        final List<InitParam> params = getParameters();
        if (null != params) {
            for (InitParam param : params) {
                if (paramName.equals(param.getName())) {
                    return param.getValue();
                }
            }
        }
        return null;
    }

    /**
     * Returns the value of the named parameter is specified, otherwise
     * <code>null</code>.
     * 
     * @param paramName
     *            the name of the parameter to return.
     * 
     * @return the value of the named parameter is specified, otherwise
     *         <code>null</code>.
     */
    public List<String> getParameterList(final String paramName) {
        final List<InitParam> params = getParameters();
        final List<String> result = new ArrayList<String>();
        if (null != params) {
            for (InitParam param : params) {
                if (paramName.equals(param.getName())) {
                    result.add(param.getValue());
                }
            }
        }
        return result;
    }

    /**
     * Returns the initialization parameters, is any, for this action.
     * 
     * @return the initialization parameters, is any, for this action.
     */
    @XmlElement(name = "init-param")
    protected List<InitParam> getParameters() {
        return parameters;
    }

    /**
     * Returns the collection of tasks to which the action belongs.
     * 
     * @return the collection of tasks to which the action belongs.
     */
    @XmlElement(name = "task")
    public List<String> getTasks() {
        return tasks;
    }

    /**
     * Sets the canonical name of the class used to executes the action.
     * 
     * @param clazz
     *            the canonical name of the class used to executes the action.
     */
    protected void setActionClass(String clazz) {
        this.actionClass = clazz;
    }

    /**
     * Sets the name by which to refer to this object.
     * 
     * @param name
     *            the name by which to refer to this object.
     */
    protected void setName(String name) {
        this.name = name;
    }

    /**
     * Sets the initialization parameters, is any, for this action.
     * 
     * @param parameters
     *            the initialization parameters, is any, for this action.
     */
    protected void setParameters(List<InitParam> parameters) {
        this.parameters = parameters;
    }

    /**
     * Sets the collection of tasks to which the action belongs.
     * 
     * @param tasks
     *            the collection of tasks to which the action belongs.
     */
    protected void setTasks(List<String> tasks) {
        this.tasks = tasks;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
