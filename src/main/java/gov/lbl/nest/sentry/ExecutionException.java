package gov.lbl.nest.sentry;

import jakarta.ejb.ApplicationException;

/**
 * This exception report that the execution of a {@link RealizedAction} instance
 * has failed.
 * 
 * @author patton
 */
@ApplicationException(rollback = true)
public class ExecutionException extends
                                Exception {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * Used by the Serialized mechanism.
     */
    private static final long serialVersionUID = -1L;

    // private static member data

    // private instance member data

    // constructors

    /**
     * Creates an instance of this class.
     */
    public ExecutionException() {
    }

    /**
     * Creates an instance of this class.
     * 
     * @param message
     *            the detailed message.
     */
    public ExecutionException(String message) {
        super(message);
    }

    /**
     * Creates an instance of this class.
     * 
     * @param cause
     *            the {@link Throwable} instance that cause this exception.
     */
    public ExecutionException(Throwable cause) {
        super(cause);
    }

    /**
     * Creates an instance of this class.
     * 
     * @param message
     *            the detailed message.
     * @param cause
     *            the {@link Throwable} instance that cause this exception.
     */
    public ExecutionException(String message,
                              Throwable cause) {
        super(message,
              cause);
    }

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
