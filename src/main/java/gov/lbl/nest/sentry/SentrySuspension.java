package gov.lbl.nest.sentry;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import jakarta.inject.Qualifier;

import gov.lbl.nest.common.suspension.Suspendable;

/**
 * The annotation is used to qualify the {@link Suspendable} instance use to
 * manage SPADE execution.
 * 
 * @author patton
 */
@Qualifier
@Retention(RUNTIME)
@Target({ TYPE,
          METHOD,
          FIELD,
          PARAMETER })
public @interface SentrySuspension {
    // Does nothing, just a label.
}
