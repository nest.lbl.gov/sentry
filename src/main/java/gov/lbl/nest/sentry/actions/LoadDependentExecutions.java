package gov.lbl.nest.sentry.actions;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.common.configure.InitializingException;
import gov.lbl.nest.common.suspension.SuspendableImpl;
import gov.lbl.nest.common.watching.ChangedItem;
import gov.lbl.nest.common.watching.Digest;
import gov.lbl.nest.jee.watching.LastWatched;
import gov.lbl.nest.jee.watching.Watcher;
import gov.lbl.nest.jee.watching.WatcherUtils;
import gov.lbl.nest.sentry.Context;
import gov.lbl.nest.sentry.ExecutionException;
import gov.lbl.nest.sentry.Invocation;
import gov.lbl.nest.sentry.RealizedAction;
import gov.lbl.nest.sentry.Sentry;
import gov.lbl.nest.sentry.config.Action;

/**
 * Sends out {@link Digest} instances for files opened by the DAQ.
 * 
 * @author patton
 */
public class LoadDependentExecutions extends
                                     SuspendableImpl implements
                                     RealizedAction {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(LoadDependentExecutions.class);

    /**
     * The default value to use for the depth of a single lookup.
     */
    private static final int DEFAULT_DEPTH = 300;

    // private static member data

    // private instance member data

    /**
     * The directory, if any, into which debug information will be saved.
     */
    private final File debugPath;

    /**
     * The name of the action that will run dependent executions later.
     */
    private String dependentExecutionAction;

    /**
     * The name of the action that will filter out items to ignore.
     */
    private String filterAction;

    /**
     * The URL of the items to consume.
     */
    private final String itemsUrl;

    /**
     * The role that this object is supplying
     */
    private final String role;

    /**
     * The maximum depth of a single lookup.
     */
    private final int selectionDepth;

    /**
     * The {@link Sentry} instance managing this object.
     */
    private final Sentry sentry;

    /**
     * The {@link Watcher} instance used by this object.
     */
    private final Watcher watcher;

    // constructors

    /**
     * Creates an instance of this class.
     * 
     * @param action
     *            the description of the instance to create.
     * @param suspend
     *            True when execution of this object should be suspended at
     *            creation.
     * @param context
     *            the {@link Context} instance suppling the execution context of
     *            the created instance.
     * @param sentry
     *            the {@link Sentry} instance managing this object.
     */
    public LoadDependentExecutions(final Action action,
                                   final Boolean suspend,
                                   final Context context,
                                   final Sentry sentry) {
        super(action.getName(),
              suspend);

        debugPath = DigestFromUrl.validateDebugPath(action.getParameter("debugPath"));

        dependentExecutionAction = action.getParameter("dependentExecutionAction");
        if (null == dependentExecutionAction) {
            throw new IllegalStateException("\"dependentExecutionAction\" must be specified in action declaration");
        }

        filterAction = action.getParameter("filterAction");

        role = action.getParameter("role");
        if (null == role) {
            throw new IllegalStateException("\"role\" must be specified in action declaration");
        }

        final String depth = action.getParameter("selectionDepth");
        if (null == depth) {
            selectionDepth = DEFAULT_DEPTH;
        } else {
            int depthToUse;
            try {
                depthToUse = Integer.parseInt(depth);
            } catch (NumberFormatException e) {
                depthToUse = DEFAULT_DEPTH;
            }
            selectionDepth = depthToUse;
        }

        String url = action.getParameter("url");
        if (null == url) {
            throw new IllegalStateException("\"url\" must be specified in action declaration");
        }
        itemsUrl = DigestFromUrl.trimmedUrl(url);

        this.sentry = sentry;
        watcher = context.getInstance(Watcher.class);
    }

    // instance member method (alphabetic)

    @SuppressWarnings("unchecked")
    @Override
    public void execute() throws ExecutionException,
                          InitializingException {
        final LastWatched last = watcher.getLastWatched(role);
        if (null == last) {
            LOG.debug("\"" + role
                      + "\" is not watching, so will not do anything");
        } else {
            final Date lastDateTime = last.getLastDateTime();
            LOG.debug("Looking for new entries for \"" + role
                      + "\" on or after "
                      + lastDateTime);
            final List<? extends ChangedItem> itemsIssued = WatcherUtils.sinceLastWatched(last,
                                                                                     DigestFromUrl.getIssuedItemsOnOrAfter(lastDateTime,
                                                                                                                           null,
                                                                                                                           selectionDepth,
                                                                                                                           itemsUrl,
                                                                                                                           LOG,
                                                                                                                           debugPath));
            if (itemsIssued.isEmpty()) {
                LOG.debug("No new entries found");
                return;
            }
            LOG.debug(itemsIssued.size() + " new entries found");
            final List<String> identities = new ArrayList<String>(itemsIssued.size());
            for (ChangedItem itemIssued : itemsIssued) {
                identities.add(itemIssued.getItem());
            }
            final List<String> filteredIdentities;
            if (null != filterAction) {
                try {
                    filteredIdentities = sentry.executeAction(filterAction,
                                                              new Invocation() {

                                                                  @Override
                                                                  public <T> T execute(RealizedAction action,
                                                                                       Class<T> clazz) {
                                                                      if (!(action instanceof ItemFilter)) {
                                                                          return null;
                                                                      }
                                                                      return (T) ((ItemFilter) action).accept(identities);
                                                                  }

                                                                  @Override
                                                                  public String getDescription() {
                                                                      return "accept";
                                                                  }
                                                              },
                                                              identities.getClass());
                    LOG.debug(filteredIdentities.size()
                              + " new entries remain after filtering");
                } catch (TimeoutException e) {
                    final String message = "As \"" + filterAction
                                           + "\" as already executing, will try again later";
                    LOG.warn(message);
                    return;
                }
            } else {
                filteredIdentities = identities;
            }
            if (null != filteredIdentities && !filteredIdentities.isEmpty()) {
                try {
                    sentry.executeAction(dependentExecutionAction,
                                         new Invocation() {

                                             @Override
                                             public <T> T execute(RealizedAction action,
                                                                  Class<T> clazz) {
                                                 if (!(action instanceof DependentExecutions)) {
                                                     return (T) Boolean.FALSE;
                                                 }
                                                 return (T) ((DependentExecutions) action).addDependentExecutions(filteredIdentities);
                                             }

                                             @Override
                                             public String getDescription() {
                                                 return "addDependentExecutions";
                                             }
                                         },
                                         Boolean.class);
                    watcher.forwardLastWatched(last,
                                               itemsIssued);
                } catch (TimeoutException e) {
                    final String message = "As \"" + dependentExecutionAction
                                           + "\" as already executing, will try again later";
                    LOG.warn(message);
                    return;
                }
            }
            watcher.forwardLastWatched(last,
                                       itemsIssued);
        }
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}

}
