package gov.lbl.nest.sentry.actions;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;

/**
 * The class represents an action that uses {@link WaitingItem} instances when
 * the correct conditions are met.
 * 
 * @author patton
 */
@Entity
@NamedQueries({ @NamedQuery(name = "getAction",
                            query = "SELECT a" + " FROM WaitingAction a"
                                    + " WHERE a.name = :name") })
public class WaitingAction {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * This object's unique identifier within the set of {@link WaitingAction}
     * instances.
     */
    private int waitingActionKey;

    /**
     * The name of the action that is waiting for file ids.
     */
    private String name;

    // constructors

    /**
     * Create an instance of this class.
     */
    protected WaitingAction() {
    }

    /**
     * Create an instance of this class.
     * 
     * @param name
     *            the name of the action that is waiting for file ids.
     */
    public WaitingAction(String name) {
        setName(name);
    }

    // instance member method (alphabetic)

    /**
     * Returns this object's unique identifier within the set of
     * {@link WaitingAction} instances.
     * 
     * @return this object's unique identifier within the set of
     *         {@link WaitingAction} instances.
     */
    @Id
    @GeneratedValue
    protected int getWaitingActionKey() {
        return waitingActionKey;
    }

    /**
     * Returns the name of the action that is waiting for file ids.
     * 
     * @return the name of the action that is waiting for file ids.
     */
    @Column(nullable = false,
            updatable = false)
    public String getName() {
        return name;
    }

    /**
     * Sets this object's unique identifier within the set of
     * {@link WaitingAction} instances.
     * 
     * @param key
     *            the unique value
     */
    protected void setWaitingActionKey(int key) {
        waitingActionKey = key;
    }

    /**
     * Sets the name of the file whose FileInfo is missing
     * 
     * @param name
     *            the name of the file whose FileInfo is missing
     */
    protected void setName(String name) {
        this.name = name;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
