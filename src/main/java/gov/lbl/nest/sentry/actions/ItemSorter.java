package gov.lbl.nest.sentry.actions;

import java.util.List;

import gov.lbl.nest.sentry.ExecutionException;

/**
 * This interface is used to sort items that should tested for dependent
 * execution.
 * 
 * @author patton
 * 
 */
public interface ItemSorter {

    /**
     * Returns a ordered list of items who should have their conditions for
     * execution assessed. The items that should be tested first should be at
     * the beginning of the list. Dependencies checking will progress through
     * the list, even if a item fails its dependency check, until all item have
     * been checked or the limit has been reached.
     * 
     * @param items
     *            the list of identifiers of the items to be sorted.
     * 
     * @return the ordered list of items who should have their conditions for
     *         execution assessed.
     * 
     * @throws ExecutionException
     *             when the execution has not completed successfully.
     */
    List<String> sort(List<String> items) throws ExecutionException;
}
