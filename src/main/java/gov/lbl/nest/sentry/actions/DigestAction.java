package gov.lbl.nest.sentry.actions;

import gov.lbl.nest.common.watching.Digest;
import gov.lbl.nest.sentry.ExecutionException;

/**
 * This interface in implemented by any class that has a method that can be
 * invoked with a {@link Digest} instance.
 * 
 * @author patton
 */
public interface DigestAction {

    /**
     * Act using the supplied {@link Digest} instance.
     * 
     * @param digest
     *            the {@link Digest} on which this method should act.
     * 
     * @return true when the action is successful.
     * 
     * @throws ExecutionException
     *             when the execution has not completed successfully.
     */
    Boolean execute(Digest digest) throws ExecutionException;
}
