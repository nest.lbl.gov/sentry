package gov.lbl.nest.sentry.actions;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.common.external.Command;
import gov.lbl.nest.common.external.ExecutionFailedException;
import gov.lbl.nest.common.suspension.SuspendableImpl;
import gov.lbl.nest.sentry.RealizedAction;
import gov.lbl.nest.sentry.config.Action;

/**
 * Executes specified script.
 * 
 * @author patton
 */
public class ExecuteScript extends
                           SuspendableImpl implements
                           RealizedAction {

    // public static final member data

    /**
     * The directory, in the destination, into which error files will be
     * written.
     */
    public static final String ERR_DIR = "err";

    /**
     * The suffix used to create the default error file name.
     */
    public static final String ERR_SUFFIX = ".err";

    /**
     * The directory, in the destination, into which log files will be written.
     */
    public static final String LOG_DIR = "log";

    /**
     * The suffix used to create the default log file name.
     */
    public static final String LOG_SUFFIX = ".log";

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ExecuteScript.class);

    /**
     * The suffix that might be used for shell scripts.
     */
    private static final String SHELL_SUFFIX = ".sh";

    /**
     * The name of the system property that contains the users home directory.
     */
    private static final String USER_HOME_PROPERTY = "user.home";

    /**
     * The UTC {@link TimeZone}, used to build log file hierarchy.
     */
    private static final TimeZone UTC_TIMEZONE = TimeZone.getTimeZone("Etc/UTC");

    // private static member data

    // private instance member data

    /**
     * The root relative to which log and error files will be written.
     */
    private final File logRoot;

    /**
     * the script to be run by this instance.
     */
    private final File script;

    // constructors

    /**
     * Creates an instance of this class.
     * 
     * @param action
     *            the description of the instance to create.
     * @param suspend
     *            True when execution of this object should be suspended at
     *            creation.
     * 
     * @throws IllegalArgumentException
     *             when not all of the required action parameters are provided.
     */
    public ExecuteScript(final Action action,
                         final Boolean suspend) throws IllegalArgumentException {
        super(action.getName(),
              suspend);

        final String outputDir = action.getParameter("output");
        final String resolvedLogRoot = getResolvedPath(action.getParameter("output"));
        if (null == resolvedLogRoot) {
            logRoot = null;
        } else {
            logRoot = new File(resolvedLogRoot);
        }
        final String resolvedScript = getResolvedPath(action.getParameter("script"));
        if (null == resolvedScript) {
            script = null;
        } else {
            script = new File(resolvedScript);
        }

        if (null == script && null == outputDir) {
            throw new IllegalArgumentException("Parameters 'script' and 'output' not specified");
        } else if (null == script) {
            throw new IllegalArgumentException("Parameters 'script' not specified");
        } else if (null == outputDir) {
            throw new IllegalArgumentException("Parameters 'output' not specified");
        }

    }

    // instance member method (alphabetic)

    @Override
    public void execute() {
        final Date now = new Date();
        final File outputDir = new File(addCalendarDirectories(logRoot,
                                                               now),
                                        LOG_DIR);
        if (!outputDir.exists()) {
            if (!outputDir.mkdirs()) {
                LOG.warn("The script output directory, \"" + outputDir.getPath()
                         + "\" could not be created.");
                return;
            }
        }
        File errorDir = new File(addCalendarDirectories(logRoot,
                                                        now),
                                 ERR_DIR);
        if (!errorDir.exists()) {
            if (!errorDir.mkdirs()) {
                LOG.warn("The script error directory, \"" + errorDir.getPath()
                         + "\" could not be created.");
                return;
            }
        }

        final String name = script.getName();
        final String nameToUse;
        if (name.endsWith(SHELL_SUFFIX)) {
            nameToUse = name.substring(0,
                                       name.length() - SHELL_SUFFIX.length());
        } else {
            nameToUse = name;
        }
        final File outputFile = new File(outputDir,
                                         getTimePrefix(now) + nameToUse
                                                    + LOG_SUFFIX);
        final File errorFile = new File(errorDir,
                                        getTimePrefix(now) + nameToUse
                                                  + ERR_SUFFIX);
        final String[] cmdArray = new String[] { "bash",
                                                 "-c",
                                                 script.getAbsolutePath() };
        final Command command = new Command(cmdArray);
        try {
            command.execute(outputFile,
                            errorFile);
            final ExecutionFailedException cmdException = command.getCmdException();
            if (null != cmdException) {
                LOG.error("Failed to execute \"" + name
                          + "\", see error file");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    // static member methods (alphabetic)

    /**
     * Extends the supplied directory by adding on calendar related
     * sub-directories.
     * 
     * @param directory
     *            the directory to extend.
     * @param dateTime
     *            the date and time to use for the extension
     * 
     * @return the extended directory.
     */
    private static File addCalendarDirectories(final File directory,
                                               final Date dateTime) {
        final Calendar calendar = new GregorianCalendar(UTC_TIMEZONE);
        calendar.setTime(dateTime);
        final File year = new File(directory,
                                   String.format("%04d",
                                                 Integer.valueOf(calendar.get(Calendar.YEAR))));
        if (!year.exists()) {
            year.mkdir();
        }
        final File month = new File(year,
                                    String.format("%02d",
                                                  Integer.valueOf(calendar.get(Calendar.MONTH)
                                                                  + 1)));
        if (!month.exists()) {
            month.mkdir();
        }
        final File day = new File(month,
                                  String.format("%02d",
                                                Integer.valueOf(calendar.get(Calendar.DAY_OF_MONTH))));
        if (!day.exists()) {
            day.mkdir();
        }
        return day;
    }

    /**
     * Returns the value of the path resolved for any leading "~".
     * 
     * @param path
     *            the path to resolve.
     * 
     * @return the value of the path resolved for any leading "~".
     */
    protected static String getResolvedPath(String path) {
        if (null != path && path.startsWith("~" + File.separator)) {
            final String userHome = System.getProperty(USER_HOME_PROPERTY);
            return userHome + path.substring(1);
        }
        return path;
    }

    /**
     * Extends the supplied directory by adding on calendar related
     * sub-directories.
     * 
     * @param directory
     *            the directory to extend.
     * @param dateTime
     *            the date and time to use for the extension
     * 
     * @return the extended directory.
     */
    private static String getTimePrefix(final Date dateTime) {
        final Calendar calendar = new GregorianCalendar(UTC_TIMEZONE);
        calendar.setTime(dateTime);
        final StringBuilder sb = new StringBuilder();
        sb.append(String.format("%02d",
                                Integer.valueOf(calendar.get(Calendar.HOUR_OF_DAY))));
        sb.append(String.format("%02d",
                                Integer.valueOf(calendar.get(Calendar.MINUTE))));
        sb.append(String.format("%02d",
                                Integer.valueOf(calendar.get(Calendar.SECOND))));
        sb.append('_');
        return sb.toString();
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
