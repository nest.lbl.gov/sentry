package gov.lbl.nest.sentry.actions;

import java.util.List;

/**
 * This interface is used to filter items that should tested for dependent
 * execution.
 * 
 * @author patton
 * 
 */
public interface ItemFilter {

    /**
     * Returns a list of items who should have their conditions for execution
     * assessed. Items not in the returned list will be completely ignored for
     * execution.
     * 
     * @param items
     *            the list of identifiers of the items to be filtered.
     * 
     * @return returns list of items who should have their conditions for
     *         execution assessed.
     */
    List<String> accept(List<String> items);
}
