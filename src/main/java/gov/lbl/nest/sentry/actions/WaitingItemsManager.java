package gov.lbl.nest.sentry.actions;

import java.util.ArrayList;
import java.util.List;

import jakarta.ejb.Stateless;
import jakarta.ejb.TransactionAttribute;
import jakarta.ejb.TransactionAttributeType;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.persistence.NoResultException;
import jakarta.persistence.Query;
import jakarta.persistence.TypedQuery;

import gov.lbl.nest.sentry.SentryDB;

/**
 * This class manages persistence for items that will be used by a
 * {@link WaitingAction} instance when the correct conditions are met.
 * 
 * @author patton
 */
@Stateless
public class WaitingItemsManager {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link List} to return when the return list does not exist.
     */
    private static final List<String> EMPTY_LIST = new ArrayList<String>();

    // private static member data

    // private instance member data

    /**
     * The {@link EntityManager} instance used by this object.
     */
    @Inject
    @SentryDB
    private EntityManager entityManager;

    // constructors

    // instance member method (alphabetic)

    /**
     * Adds the specified list of items to those waiting to be used by the
     * supplied action.
     * 
     * @param action
     *            the action which will use these items when the conditions are
     *            met.
     * @param identities
     *            the list identities of items to be added to those waiting to
     *            be used by the supplied action.
     */
    // This requires a new transaction as it may be called while in a
    // transaction accessing a different DB.
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void addAsWaiting(final String action,
                             final List<String> identities) {
        final WaitingAction waitingAction = getAction(action);
        final TypedQuery<String> query = entityManager.createNamedQuery("getSpecifiedItemsForAction",
                                                                        String.class);
        query.setParameter("action",
                           waitingAction);
        query.setParameter("identities",
                           identities);
        final List<String> existing = query.getResultList();
        for (String name : identities) {
            if (null == existing || !existing.contains(name)) {
                entityManager.persist(new WaitingItem(waitingAction,
                                                      name));
            }
        }
    }

    /**
     * Returns the {@link WaitingAction} instance with the specified name,
     * creating it if necessary.
     * 
     * @param action
     *            the name of the {@link WaitingAction} instance to return.
     * 
     * @return the {@link WaitingAction} instance with the specified name,
     *         creating it if necessary.
     */
    protected WaitingAction getAction(String action) {
        final TypedQuery<WaitingAction> query = entityManager.createNamedQuery("getAction",
                                                                               WaitingAction.class);
        query.setParameter("name",
                           action);
        try {
            return query.getSingleResult();
        } catch (NoResultException e) {
            final WaitingAction result = new WaitingAction(action);
            entityManager.persist(result);
            return result;
        }
    }

    /**
     * Returns the list of identities of items that are waiting to be used by
     * the supplied action.
     * 
     * @param action
     *            the action whose list of waiting items should be returned.
     * 
     * @return the list of identities of items that are waiting to be used by
     *         the supplied action.
     */
    // This requires a new transaction as it may be called while in a
    // transaction accessing a different DB.
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public List<String> getWaiting(String action) {
        final WaitingAction waitingAction = getAction(action);
        final TypedQuery<String> query = entityManager.createNamedQuery("getItemsForAction",
                                                                        String.class);
        query.setParameter("action",
                           waitingAction);
        final List<String> result = query.getResultList();
        if (null == result) {
            return EMPTY_LIST;
        }
        return result;
    }

    /**
     * Clears the list of identities of items that are waiting to be used by the
     * supplied action, with the exception of those in the supplied list.
     * 
     * @param action
     *            the action whose list of waiting items should be cleared.
     * @param identities
     *            the list of identities of items not to clear from the list.
     * 
     * @return the number of items that were removed from the list.
     */
    // This requires a new transaction as it may be called while in a
    // transaction accessing a different DB.
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public int stillWaiting(String action,
                            List<String> identities) {
        final WaitingAction waitingAction = getAction(action);
        if (null == identities || identities.isEmpty()) {
            final Query query = entityManager.createNamedQuery("deleteItemsForAction");
            query.setParameter("action",
                               waitingAction);
            return query.executeUpdate();
        }
        final TypedQuery<WaitingItem> query = entityManager.createNamedQuery("excludedItems",
                                                                             WaitingItem.class);
        query.setParameter("action",
                           waitingAction);
        query.setParameter("identities",
                           identities);
        final List<WaitingItem> updated = query.getResultList();
        int result = 0;
        if (null != updated) {
            for (WaitingItem waitingItem : updated) {
                entityManager.remove(waitingItem);
                result++;
            }
        }
        return result;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
