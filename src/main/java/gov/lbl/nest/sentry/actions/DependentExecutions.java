package gov.lbl.nest.sentry.actions;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.common.configure.InitializingException;
import gov.lbl.nest.common.external.Command;
import gov.lbl.nest.common.external.ExecutionFailedException;
import gov.lbl.nest.common.suspension.SuspendableImpl;
import gov.lbl.nest.sentry.Context;
import gov.lbl.nest.sentry.ExecutionException;
import gov.lbl.nest.sentry.Invocation;
import gov.lbl.nest.sentry.RealizedAction;
import gov.lbl.nest.sentry.Sentry;
import gov.lbl.nest.sentry.config.Action;

/**
 * This class executed an action if the conditions for its execution are
 * satisfied.
 * 
 * @author patton
 */
public class DependentExecutions extends
                                 SuspendableImpl implements
                                 RealizedAction {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(DependentExecutions.class);

    /**
     * The default maximum number of executions in a single invocation of this
     * action.
     */
    private static final int DEFAULT_MAX = 32;

    /**
     * THe object to use when an empty list is needed.
     */
    private static final List<String> EMPTY_LIST = new ArrayList<String>();

    // private static member data

    // private instance member data

    /**
     * The maximum number of executions in a single invocation of this action.
     */
    private final int max;

    /**
     * The name used to refer to this object.
     */
    private final String name;

    /**
     * The script to execute on items waiting for their conditions to be met.
     */
    private final String scriptCmd;

    /**
     * The {@link Sentry} instance managing this object.
     */
    private final Sentry sentry;

    /**
     * The name of the action, if any, that will sort the items before checking
     * dependencies.
     */
    private String sortingAction;

    /**
     * The {@link WaitingItemsManager} instance used by this object
     */
    private WaitingItemsManager waitingFilesManager;

    // constructors

    /**
     * Creates an instance of this class.
     * 
     * @param action
     *            the description of the instance to create.
     * @param suspend
     *            True when execution of this object should be suspended at
     *            creation.
     * @param context
     *            the {@link Context} instance suppling the execution context of
     *            the created instance.
     * @param sentry
     *            the {@link Sentry} instance managing this object.
     */
    public DependentExecutions(final Action action,
                               final Boolean suspend,
                               final Context context,
                               final Sentry sentry) {
        super(action.getName(),
              suspend);

        name = action.getName();
        scriptCmd = action.getParameter("command");
        if (null == scriptCmd) {
            throw new IllegalStateException("\"scriptCmd\" must be specified in action declaration");
        }

        try {
            int maxToUse;
            try {
                maxToUse = Integer.parseInt(action.getParameter("max"));
            } catch (NumberFormatException e) {
                maxToUse = DEFAULT_MAX;
            }
            max = maxToUse;
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("\"max\" must be an integer");
        }

        sortingAction = action.getParameter("sortingAction");

        this.sentry = sentry;

        waitingFilesManager = context.getInstance(WaitingItemsManager.class);
    }

    // instance member method (alphabetic)

    /**
     * Adds the supplied list of items to the set of items awaiting execution.
     * If the item is already in the set then it will not be added a second
     * time.
     * 
     * @param items
     *            the list of items to add to the set awaiting execution.
     * 
     * @return true when successfully completed.
     */
    public Boolean addDependentExecutions(List<String> items) {
        if (null == items || items.isEmpty()) {
            return true;
        }

        final List<String> notUpdated = executeScript(items);
        if (!notUpdated.isEmpty()) {
            waitingFilesManager.addAsWaiting(name,
                                             notUpdated);
            final String plural;
            if (1 == notUpdated.size()) {
                plural = " has";
            } else {
                plural = "s have";
            }
            LOG.info(notUpdated.size() + " new item"
                     + plural
                     + " been added to the set awaiting execution");
        }
        return true;
    }

    @Override
    public void execute() throws ExecutionException,
                          InitializingException {
        final List<String> items = getWaiting();
        if (items.isEmpty()) {
            return;
        }

        final List<String> notUpdated = executeScript(items);
        waitingFilesManager.stillWaiting(name,
                                         notUpdated);
        if (!notUpdated.isEmpty()) {
            final String plural;
            final String pronoun;
            if (1 == notUpdated.size()) {
                plural = " is";
                pronoun = "its";
            } else {
                plural = "s are";
                pronoun = "their";
            }
            LOG.info(notUpdated.size() + " item"
                     + plural
                     + " still waiting for the conditions of "
                     + pronoun
                     + " execution to be satisfied");
        }
    }

    /**
     * Executes this object's script with the supplied list of items passed in
     * via the stdin.
     * 
     * @param items
     *            list of items to be passed to this object's script via the
     *            stdin.
     */
    private List<String> executeScript(List<String> items) {
        final String[] cmdArray = new String[] { "bash",
                                                 "-c",
                                                 scriptCmd };
        final Command command = new Command(cmdArray);
        final StringBuilder stringBuilder = new StringBuilder();
        final List<String> listToUse;
        final List<String> unusedList;
        if (items.size() > max) {
            listToUse = items.subList(0,
                                      max);
            unusedList = items.subList(max,
                                       items.size());
        } else {
            listToUse = items;
            unusedList = EMPTY_LIST;
        }
        for (String item : listToUse) {
            stringBuilder.append(item + "\n");
        }
        final String inputString = stringBuilder.toString();
        final InputStream inputStream = new ByteArrayInputStream(inputString.getBytes());
        try {
            command.execute(null,
                            null,
                            inputStream);
            final ExecutionFailedException cmdException = command.getCmdException();
            if (null != cmdException) {
                throw cmdException;
            }
            final List<String> result = new ArrayList<String>(command.getStdOut());
            result.addAll(unusedList);
            return result;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionFailedException e) {
            final String[] stderr = e.getStdErr();
            if (null == stderr || 0 == stderr.length) {
                LOG.error("No Standard error output for \"" + e.getCmdString()
                          + "\"");
            } else {
                LOG.error("Standard error output for \"" + e.getCmdString()
                          + "\"");
                for (String line : stderr) {
                    LOG.error("  " + line);
                }
            }
            e.printStackTrace();
        }
        return items;
    }

    /**
     * Returns the list of identities of items that are waiting to be used by
     * this object.
     * 
     * @return the list of identities of items that are waiting to be used by
     *         this object
     * 
     * @throws ExecutionException
     *             when the execution has not completed successfully.
     * @throws InitializingException
     *             when the application is still initializing.
     */
    @SuppressWarnings("unchecked")
    public List<String> getWaiting() throws ExecutionException,
                                     InitializingException {
        final List<String> waiting = waitingFilesManager.getWaiting(name);
        if (null == sortingAction) {
            return waiting;
        }
        try {
            return sentry.executeAction(sortingAction,
                                        new Invocation() {

                                            @Override
                                            public <T> T execute(RealizedAction action,
                                                                 Class<T> clazz) throws ExecutionException {
                                                if (!(action instanceof ItemSorter)) {
                                                    return null;
                                                }
                                                return (T) ((ItemSorter) action).sort(waiting);
                                            }

                                            @Override
                                            public String getDescription() {
                                                return "accept";
                                            }
                                        },
                                        waiting.getClass());
        } catch (TimeoutException e) {
            final String message = "As \"" + sortingAction
                                   + "\" as already executing, will try again later";
            LOG.warn(message);
            return EMPTY_LIST;
        }
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
