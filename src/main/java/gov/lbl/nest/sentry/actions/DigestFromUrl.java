package gov.lbl.nest.sentry.actions;

import java.io.File;
import java.io.FileNotFoundException;
import java.lang.reflect.Constructor;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import jakarta.mail.Session;
import jakarta.xml.bind.JAXBContext;
import jakarta.xml.bind.JAXBException;
import jakarta.xml.bind.Marshaller;
import jakarta.xml.bind.UnmarshalException;
import jakarta.xml.bind.Unmarshaller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.common.rs.QueryDate;
import gov.lbl.nest.common.suspension.SuspendableImpl;
import gov.lbl.nest.common.watching.ChangedItem;
import gov.lbl.nest.common.watching.ChangedItemFilter;
import gov.lbl.nest.common.watching.Digest;
import gov.lbl.nest.common.watching.DigestChange;
import gov.lbl.nest.jee.watching.LastWatched;
import gov.lbl.nest.jee.watching.Watcher;
import gov.lbl.nest.jee.watching.WatcherUtils;
import gov.lbl.nest.jee.watching.WatchingException;
import gov.lbl.nest.sentry.Context;
import gov.lbl.nest.sentry.ExecutionException;
import gov.lbl.nest.sentry.RealizedAction;
import gov.lbl.nest.sentry.config.Action;

/**
 * Sends out {@link Digest} instances for {@link Digest} instances obtained from
 * URL.
 * 
 * @author patton
 */
public class DigestFromUrl extends
                           SuspendableImpl implements
                           RealizedAction {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(DigestFromUrl.class);

    /**
     * The default value to use for the depth of a single lookup.
     */
    private static final int DEFAULT_DEPTH = 300;

    /**
     * The date format to use for debug filenames.
     */
    private static final String DEBUG_DATE_FORMAT = "yyyyMMdd_HHmm";

    /**
     * The formatter for creating debug filenames
     */
    private static final DateFormat DEBUG_DATE_FORMATTER = new SimpleDateFormat(DEBUG_DATE_FORMAT);

    /**
     * The prefix used on debug filenames.
     */
    private static final String DEBUG_FILE_PREFIX = "DEBUG_";

    /**
     * The suffix used on debug files.
     */
    private static final String DEBUG_SUFFIX = ".xml";

    /**
     * Value to use when the file list is empty.
     */
    private static final List<DigestChange> EMPTY_FILE_LIST = new ArrayList<DigestChange>();

    /**
     * The separator used in URLs.
     */
    private static final String URL_SEPARATOR = "/";

    /**
     * The name of the system property that contains the users home directory.
     */
    private static final String USER_HOME_PROPERTY = "user.home";

    /**
     * The string used to signify the users home directory.
     */
    private static final String USER_HOME_STRING = "~";

    /**
     * The {@link Marshaller} property to set formatting.
     */
    private static final String XML_FORMAT_PROPERTY = "jaxb.formatted.output";

    // private static member data

    // private instance member data

    /**
     * The directory, if any, into which debug information will be saved.
     */
    private final File debugPath;

    /**
     * The email to which the {@link Digest} should be sent.
     */
    private final String email;

    /**
     * The {@link ChangedItemFilter} instance, if any, used by this object.
     */
    private ChangedItemFilter filter;

    /**
     * The URL to which the {@link Digest} should be POSTed.
     */
    private final URL post;

    /**
     * The role that this object is supplying
     */
    private final String role;

    /**
     * The {@link Session} used by this object.
     */
    private final Session session;

    /**
     * The maximum depth of a single lookup.
     */
    private final int selectionDepth;

    /**
     * The subject to use in the resulting digest.
     */
    private final String subject;

    /**
     * The URL of the collection of watched items.
     */
    private final String url;

    /**
     * The {@link Watcher} instance used by this object.
     */
    private final Watcher watcher;

    // constructors

    /**
     * Creates an instance of this class.
     * 
     * @param action
     *            the description of the instance to create.
     * @param suspend
     *            True when execution of this object should be suspended at
     *            creation.
     * @param context
     *            the {@link Context} instance suppling the execution context of the
     *            created instance.
     * 
     * @throws IllegalArgumentException
     *             when a requested filter class can not be instantiated.
     */
    public DigestFromUrl(final Action action,
                         final Boolean suspend,
                         final Context context) throws IllegalArgumentException {
        super(action.getName(),
              suspend);

        debugPath = validateDebugPath(action.getParameter("debugPath"));

        email = action.getParameter("email");
        final String possiblePost = action.getParameter("post");
        if (null == email && null == possiblePost) {
            throw new IllegalStateException("\"email\" or \"post\" must be specified in action declaration");
        }
        if (null != email && null != possiblePost) {
            throw new IllegalStateException("Either \"email\" or \"post\" must be specified in action declaration, but not both");
        }
        if (null == possiblePost) {
            post = null;
        } else {
            try {
                post = new URL(possiblePost);
            } catch (MalformedURLException e1) {
                throw new IllegalStateException("\"post\" value is not a valid URL");
            }
        }

        final String filterClass = action.getParameter("filter");
        if (null == filterClass) {
            filter = null;
        } else {
            final ClassLoader loader = getClass().getClassLoader();
            try {
                final Class<?> clazz = loader.loadClass(filterClass);
                final Constructor<?> constructor = clazz.getConstructor();
                filter = (ChangedItemFilter) constructor.newInstance();
            } catch (Exception e) {
                throw new IllegalArgumentException("Unable to instantiate \"" + filterClass
                                                   + "\"",
                                                   e);
            }
        }

        role = action.getParameter("role");
        if (null == role) {
            throw new IllegalStateException("\"role\" must be specified in action declaration");
        }

        final String depth = action.getParameter("selectionDepth");
        if (null == depth) {
            selectionDepth = DEFAULT_DEPTH;
        } else {
            int depthToUse;
            try {
                depthToUse = Integer.parseInt(depth);
            } catch (NumberFormatException e) {
                depthToUse = DEFAULT_DEPTH;
            }
            selectionDepth = depthToUse;
        }

        subject = action.getParameter("subject");
        if (null == subject) {
            throw new IllegalStateException("\"subject\" must be specified in action declaration");
        }

        url = action.getParameter("url");
        if (null == url) {
            throw new IllegalStateException("\"url\" must be specified in action declaration");
        }

        session = context.getInstance(Session.class);
        watcher = context.getInstance(Watcher.class);
    }

    // instance member method (alphabetic)

    @Override
    public void execute() throws ExecutionException {
        final LastWatched last = watcher.getLastWatched(role);
        if (null == last) {
            LOG.debug("\"" + role
                      + "\" is not watching, so will not do anything");
        } else {
            final Date lastDateTime = last.getLastDateTime();
            LOG.debug("Looking for new entries for \"" + role
                      + "\" on or after "
                      + lastDateTime);
            final List<? extends ChangedItem> processed = watcher.forwardLastWatched(last,
                                                                                     getIssuedItemsOnOrAfter(lastDateTime,
                                                                                                             null,
                                                                                                             selectionDepth,
                                                                                                             url,
                                                                                                             LOG,
                                                                                                             debugPath));
            final List<? extends ChangedItem> filtered;
            if (null == filter) {
                filtered = processed;
            } else {
                filtered = filter.filter(processed);
            }
            if (!filtered.isEmpty()) {
                final Digest digest = new Digest(subject,
                                                 filtered);
                if (null == post) {
                    try {
                        WatcherUtils.send(digest,
                                     session,
                                     email);
                        LOG.info("Emailed digest for \"" + last.getRole()
                                 + "\" to \""
                                 + email
                                 + "\"");
                    } catch (WatchingException e) {
                        throw new ExecutionException("Failed to POST digest for \"" + last.getRole()
                                                     + "\" to \""
                                                     + post
                                                     + "\"",
                                                     e);
                    }
                } else {
                    try {
                        WatcherUtils.post(digest,
                                     post);
                        LOG.info("POSTed digest for \"" + last.getRole()
                                 + "\" to \""
                                 + post
                                 + "\"");
                    } catch (WatchingException e) {
                        throw new ExecutionException("Failed to POST digest for \"" + last.getRole()
                                                     + "\" to \""
                                                     + post
                                                     + "\"",
                                                     e);
                    }
                }
            }
        }
    }

    // static member methods (alphabetic)

    /**
     * Returns the list of items issued at the specified URL, on or after the
     * supplied date and time.
     * 
     * @param after
     *            the time on or after which items should be include in the returned
     *            list.
     * @param before
     *            the time before which items should be include in the returned
     *            list. If <code>null</code> then no time limit is used.
     * @param maxCount
     *            the maximum number of items to return, 0 is unlimited.
     * @param url
     *            the URL of the collection of items.
     * @param log
     *            the {@link Logger} instance to used within this method.
     * @param debugPath
     *            the directory, if any, into which debug information will be saved.
     * 
     * @return the list of items issued at the specified URL, on or after the
     *         supplied date and time.
     */
    public static List<? extends ChangedItem> getIssuedItemsOnOrAfter(final Date after,
                                                                      final Date before,
                                                                      final int maxCount,
                                                                      final String url,
                                                                      final Logger log,
                                                                      final File debugPath) {
        final String afterDate = QueryDate.LONG_ZONED_FORMATTER.format(after);
        final String conjunction;
        if (-1 == url.indexOf("?")) {
            conjunction = "?";
        } else {
            conjunction = "&";
        }
        final String beforeQuery;
        if (null == before) {
            beforeQuery = "";
        } else {
            final String beforeDate = QueryDate.LONG_ZONED_FORMATTER.format(before);
            beforeQuery = "&before=" + beforeDate.replaceFirst("\\+",
                                                               "%2B");
        }
        final String finalUrl = url + conjunction
                                + "after="
                                + afterDate.replaceFirst("\\+",
                                                         "%2B")
                                + beforeQuery
                                + "&max="
                                + Integer.toString(maxCount);
        log.info("URL for watched items is \"" + finalUrl
                 + "\"");
        try {
            final JAXBContext jc = JAXBContext.newInstance(Digest.class);
            final Unmarshaller u = jc.createUnmarshaller();
            URL watchedUrl = new URL(finalUrl);
            Digest digest;
            digest = (Digest) u.unmarshal(watchedUrl);
            save(digest,
                 debugPath);
            final List<? extends DigestChange> lines = digest.getIssued();
            if (null == lines) {
                return EMPTY_FILE_LIST;
            }
            return lines;
        } catch (MalformedURLException e1) {
            // Should never get here
            e1.printStackTrace();
            return EMPTY_FILE_LIST;
        } catch (UnmarshalException e) {
            final Throwable t = e.getLinkedException();
            if (t instanceof FileNotFoundException) {
                log.warn("URL \"" + finalUrl
                         + "\" failed");
            } else {
                e.printStackTrace();
            }
            return EMPTY_FILE_LIST;
        } catch (JAXBException e) {
            e.printStackTrace();
            return EMPTY_FILE_LIST;
        }
    }

    /**
     * Saves a {@link Digest} instance into the debug directory if it has been
     * specified.
     * 
     * @param digest
     *            the {@link Digest} instance to save
     * @param debugPath
     *            the directory, if any, into which debug information will be saved.
     */
    private static void save(Digest digest,
                             File debugPath) throws JAXBException {
        if (null == debugPath) {
            return;
        }

        int count = 0;
        final String dateTime = DEBUG_DATE_FORMATTER.format(new Date());
        File file = new File(debugPath,
                             DEBUG_FILE_PREFIX + dateTime
                                        + DEBUG_SUFFIX);
        if (file.exists()) {
            ++count;
            file = new File(debugPath,
                            DEBUG_FILE_PREFIX + dateTime
                                       + "_"
                                       + count
                                       + DEBUG_SUFFIX);
        }
        JAXBContext content = JAXBContext.newInstance(digest.getClass());
        Marshaller marshaller = content.createMarshaller();
        marshaller.setProperty(XML_FORMAT_PROPERTY,
                               Boolean.TRUE);
        marshaller.marshal(digest,
                           file);
    }

    /**
     * Trims any trailing separator from the supplied URL.
     * 
     * @param url
     *            the URL to trim.
     * 
     * @return the trimmed URL.
     */
    public static String trimmedUrl(String url) {
        if (url.endsWith(URL_SEPARATOR)) {
            return url.substring(0,
                                 url.length() - URL_SEPARATOR.length());
        }
        return url;
    }

    /**
     * Returns the validate directory in which to store debug information, or
     * <code>null</code> is there isn't one.
     * 
     * @param path
     *            the path that should be validated.
     * 
     * @return the validate directory in which to store debug information, or
     *         <code>null</code> is there isn't one.
     */
    public static File validateDebugPath(final String path) {
        if (null == path) {
            return null;
        }
        final String pathToUse;
        if (path.startsWith(USER_HOME_STRING + File.separator)) {
            final String userHome = System.getProperty(USER_HOME_PROPERTY);
            pathToUse = userHome + path.substring(USER_HOME_STRING.length());
        } else {
            pathToUse = path;
        }
        File result = new File(pathToUse);
        if (!result.exists()) {
            result.mkdirs();
        }
        return result;
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}

}
