package gov.lbl.nest.sentry.actions;

import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;
import jakarta.persistence.Transient;

import gov.lbl.nest.common.watching.ChangedItem;

/**
 * The class represents an item with an associated time stamp that will be used
 * by a {@link WaitingAction} instance when the correct conditions are met.
 * 
 * @author patton
 */
@Entity
@NamedQueries({ @NamedQuery(name = "excludedChangedItems",
                            query = "SELECT i" + " FROM WaitingChangedItem i"
                                    + " WHERE i.action = :action"
                                    + " AND i.identity NOT IN (:identities)"),
                @NamedQuery(name = "deleteChangedItemsForAction",
                            query = "DELETE" + " FROM WaitingChangedItem i"
                                    + " WHERE i.action = :action"),
                @NamedQuery(name = "getChangedItemsForAction",
                            query = "SELECT i" + " FROM WaitingChangedItem i"
                                    + " WHERE i.action = :action"),
                @NamedQuery(name = "getSpecifiedChangedItemsForAction",
                            query = "SELECT i.identity"
                                    + " FROM WaitingChangedItem i"
                                    + " WHERE i.action = :action"
                                    + " AND i.identity IN (:identities)") })
public class WaitingChangedItem implements
                                ChangedItem {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The {@link WaitingAction} that is waiting to use this item.
     */
    private WaitingAction action;

    /**
     * The the identity of the item that changed.
     */
    private String identity;

    /**
     * The date and time associated with the item.
     */
    private Date timestamp;

    /**
     * This object's unique identifier within the set of
     * {@link WaitingChangedItem} instances.
     */
    private int waitingItemKey;

    // constructors

    /**
     * Create an instance of this class.
     */
    protected WaitingChangedItem() {
    }

    /**
     * Create an instance of this class.
     * 
     * @param action
     *            the {@link WaitingAction} that is waiting to use this item.
     * @param identity
     *            the identity of the item.
     * @param timestamp
     *            the date and time associated with the item.
     */
    public WaitingChangedItem(final WaitingAction action,
                              final ChangedItem item) {
        setAction(action);
        setIdentity(item.getItem());
        setTimestamp(item.getWhenItemChanged());
    }

    // instance member method (alphabetic)

    @Override
    public Object clone() throws CloneNotSupportedException {
        throw new CloneNotSupportedException();
    }

    @Override
    public int compareTo(ChangedItem o) {
        return getItem().compareTo(o.getItem());
    }

    /**
     * Returns the {@link WaitingAction} that is waiting to use this item.
     * 
     * @return the {@link WaitingAction} that is waiting to use this item.
     */
    @ManyToOne
    public WaitingAction getAction() {
        return action;
    }

    /**
     * Returns the identity of the item that changed.
     * 
     * @return the identity of the item that changed.
     */
    @Column(nullable = false,
            updatable = false)
    public String getIdentity() {
        return identity;
    }

    @Override
    @Transient
    public String getItem() {
        return getIdentity();
    }

    /**
     * Returns the date and time associated with the item.
     * 
     * @return the date and time associated with the item.
     */
    public Date getTimestamp() {
        return timestamp;
    }

    /**
     * Returns this object's unique identifier within the set of
     * {@link WaitingChangedItem} instances.
     * 
     * @return this object's unique identifier within the set of
     *         {@link WaitingChangedItem} instances.
     */
    @Id
    @GeneratedValue
    protected int getWaitingItemKey() {
        return waitingItemKey;
    }

    @Override
    @Transient
    public Date getWhenItemChanged() {
        return getTimestamp();
    }

    /**
     * Sets the {@link WaitingAction} that is waiting to use this item.
     * 
     * @param action
     *            the {@link WaitingAction} that is waiting to use this item.
     */
    protected void setAction(WaitingAction action) {
        this.action = action;
    }

    /**
     * Sets the identity of the item that changed.
     * 
     * @param id
     *            the identity of the item that changed.
     */
    protected void setIdentity(String id) {
        identity = id;
    }

    /**
     * Sets the date and time associated with the item.
     * 
     * @param timestamp
     *            the date and time associated with the item.
     */
    protected void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * Sets this object's unique identifier within the set of
     * {@link WaitingChangedItem} instances.
     * 
     * @param key
     *            the unique value
     */
    protected void setWaitingItemKey(int key) {
        waitingItemKey = key;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
