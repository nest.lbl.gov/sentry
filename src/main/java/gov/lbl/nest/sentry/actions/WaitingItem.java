package gov.lbl.nest.sentry.actions;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.NamedQueries;
import jakarta.persistence.NamedQuery;

/**
 * The class represents an item that will be used by a {@link WaitingAction}
 * instance when the correct conditions are met.
 * 
 * @author patton
 */
@Entity
@NamedQueries({ @NamedQuery(name = "excludedItems",
                            query = "SELECT i" + " FROM WaitingItem i"
                                    + " WHERE i.action = :action"
                                    + " AND i.identity NOT IN (:identities)"),
                @NamedQuery(name = "deleteItemsForAction",
                            query = "DELETE" + " FROM WaitingItem i"
                                    + " WHERE i.action = :action"),
                @NamedQuery(name = "getItemsForAction",
                            query = "SELECT i.identity" + " FROM WaitingItem i"
                                    + " WHERE i.action = :action"),
                @NamedQuery(name = "getSpecifiedItemsForAction",
                            query = "SELECT i.identity" + " FROM WaitingItem i"
                                    + " WHERE i.action = :action"
                                    + " AND i.identity IN (:identities)") })
public class WaitingItem {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The {@link WaitingAction} that is waiting to use this item.
     */
    private WaitingAction action;

    /**
     * The identity of the item.
     */
    private String identity;

    /**
     * This object's unique identifier within the set of {@link WaitingItem}
     * instances.
     */
    private int waitingItemKey;

    // constructors

    /**
     * Create an instance of this class.
     */
    protected WaitingItem() {
    }

    /**
     * Create an instance of this class.
     * 
     * @param action
     *            the {@link WaitingAction} that is waiting to use this item.
     * @param identity
     *            the identity of the item.
     */
    public WaitingItem(WaitingAction action,
                       String identity) {
        setAction(action);
        setIdentity(identity);
    }

    // instance member method (alphabetic)

    /**
     * Returns the {@link WaitingAction} that is waiting to use this item.
     * 
     * @return the {@link WaitingAction} that is waiting to use this item.
     */
    @ManyToOne
    public WaitingAction getAction() {
        return action;
    }

    /**
     * Returns the identity of the item.
     * 
     * @return the identity of the item.
     */
    @Column(nullable = false,
            updatable = false)
    public String getIdentity() {
        return identity;
    }

    /**
     * Returns this object's unique identifier within the set of
     * {@link WaitingItem} instances.
     * 
     * @return this object's unique identifier within the set of
     *         {@link WaitingItem} instances.
     */
    @Id
    @GeneratedValue
    protected int getWaitingItemKey() {
        return waitingItemKey;
    }

    /**
     * Sets the {@link WaitingAction} that is waiting to use this item.
     * 
     * @param action
     *            the {@link WaitingAction} that is waiting to use this item.
     */
    protected void setAction(WaitingAction action) {
        this.action = action;
    }

    /**
     * Sets the identity of the item.
     * 
     * @param id
     *            the identity of the item.
     */
    protected void setIdentity(String id) {
        identity = id;
    }

    /**
     * Sets this object's unique identifier within the set of
     * {@link WaitingItem} instances.
     * 
     * @param key
     *            the unique value
     */
    protected void setWaitingItemKey(int key) {
        waitingItemKey = key;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
