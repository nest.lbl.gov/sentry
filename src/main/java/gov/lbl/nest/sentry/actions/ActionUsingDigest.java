package gov.lbl.nest.sentry.actions;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.common.configure.InitializingException;
import gov.lbl.nest.common.suspension.SuspendableImpl;
import gov.lbl.nest.common.watching.ChangedItem;
import gov.lbl.nest.common.watching.Digest;
import gov.lbl.nest.jee.watching.LastWatched;
import gov.lbl.nest.jee.watching.Watcher;
import gov.lbl.nest.jee.watching.WatcherUtils;
import gov.lbl.nest.sentry.Context;
import gov.lbl.nest.sentry.ExecutionException;
import gov.lbl.nest.sentry.Invocation;
import gov.lbl.nest.sentry.RealizedAction;
import gov.lbl.nest.sentry.Sentry;
import gov.lbl.nest.sentry.config.Action;

/**
 * Sends out {@link Digest} instances for files opened by the DAQ.
 * 
 * @author patton
 */
public class ActionUsingDigest extends
                               SuspendableImpl implements
                               RealizedAction {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ActionUsingDigest.class);

    /**
     * The default value to use for the depth of a single lookup.
     */
    private static final int DEFAULT_DEPTH = 300;

    // private static member data

    // private instance member data

    /**
     * The minimum time between an item being issued and the action executing on
     * that item.
     */
    private final Long cushion;

    /**
     * The directory, if any, into which debug information will be saved.
     */
    private final File debugPath;

    /**
     * The name of the action that will run dependent executions later.
     */
    private String digestAction;

    /**
     * The URL of the items to consume.
     */
    private final String itemsUrl;

    /**
     * The role that this object is supplying
     */
    private final String role;

    /**
     * The maximum depth of a single lookup.
     */
    private final int selectionDepth;

    /**
     * The {@link Sentry} instance managing this object.
     */
    private Sentry sentry;

    /**
     * The {@link Watcher} instance used by this object.
     */
    private final Watcher watcher;

    // constructors

    /**
     * Creates an instance of this class.
     * 
     * @param action
     *            the description of the instance to create.
     * @param suspend
     *            True when execution of this object should be suspended at
     *            creation.
     * @param context
     *            the {@link Context} instance suppling the execution context of
     *            the created instance.
     * @param sentry
     *            the {@link Sentry} instance managing this object.
     */
    public ActionUsingDigest(final Action action,
                             final Boolean suspend,
                             final Context context,
                             final Sentry sentry) {
        super(action.getName(),
              suspend);

        final String requestedCushion = action.getParameter("cushion");
        final Long validatedCushion = parseCushion(requestedCushion);
        if (null == validatedCushion && null != requestedCushion) {
            cushion = (new Date()).getTime();
        } else {
            cushion = validatedCushion;
        }

        debugPath = DigestFromUrl.validateDebugPath(action.getParameter("debugPath"));

        digestAction = action.getParameter("digestAction");
        if (null == digestAction) {
            throw new IllegalStateException("\"digestAction\" must be specified in action declaration");
        }

        role = action.getParameter("role");
        if (null == role) {
            throw new IllegalStateException("\"role\" must be specified in action declaration");
        }

        final String depth = action.getParameter("selectionDepth");
        if (null == depth) {
            selectionDepth = DEFAULT_DEPTH;
        } else {
            int depthToUse;
            try {
                depthToUse = Integer.parseInt(depth);
            } catch (NumberFormatException e) {
                depthToUse = DEFAULT_DEPTH;
            }
            selectionDepth = depthToUse;
        }

        String url = action.getParameter("url");
        if (null == url) {
            throw new IllegalStateException("\"url\" must be specified in action declaration");
        }
        itemsUrl = DigestFromUrl.trimmedUrl(url);

        this.sentry = sentry;
        watcher = context.getInstance(Watcher.class);
    }

    // instance member method (alphabetic)

    @Override
    public void execute() throws ExecutionException,
                          InitializingException {
        final LastWatched last = watcher.getLastWatched(role);
        if (null == last) {
            LOG.debug("\"" + role
                      + "\" is not watching, so will not do anything");
        } else {
            LOG.debug("Using role \"" + role
                      + "\" to watch for changes");
            final Date before;
            if (null == cushion) {
                before = null;
            } else {
                before = new Date((new Date()).getTime() - cushion);
            }
            final Date lastDateTime = last.getLastDateTime();
            LOG.debug("Looking for new entries for \"" + role
                      + "\" on or after "
                      + lastDateTime);
            final List<? extends ChangedItem> itemsIssued = WatcherUtils.sinceLastWatched(last,
                                                                                     DigestFromUrl.getIssuedItemsOnOrAfter(lastDateTime,
                                                                                                                           before,
                                                                                                                           selectionDepth,
                                                                                                                           itemsUrl,
                                                                                                                           LOG,
                                                                                                                           debugPath));
            LOG.info(itemsIssued.size() + " new items found");
            if (itemsIssued.isEmpty()) {
                return;
            }
            final Date end;
            final ChangedItem lastChange = itemsIssued.get(itemsIssued.size()
                                                           - 1);
            end = lastChange.getWhenItemChanged();
            final Digest digest = new Digest("Digest built by Sentry",
                                             lastDateTime,
                                             end,
                                             itemsIssued);
            final List<String> identities = new ArrayList<String>(itemsIssued.size());
            for (ChangedItem itemIssued : itemsIssued) {
                identities.add(itemIssued.getItem());
            }
            try {
                LOG.info("Executing action");
                sentry.executeAction(digestAction,
                                     new Invocation() {

                                         @Override
                                         @SuppressWarnings("unchecked")
                                         public <T> T execute(RealizedAction action,
                                                              Class<T> clazz) throws ExecutionException {
                                             if (!(action instanceof DigestAction)) {
                                                 return (T) Boolean.FALSE;
                                             }
                                             return (T) ((DigestAction) action).execute(digest);
                                         }

                                         @Override
                                         public String getDescription() {
                                             return "Acting on Digest";
                                         }
                                     },
                                     Boolean.class);
                watcher.forwardLastWatched(last,
                                           itemsIssued);
            } catch (TimeoutException e) {
                final String message = "As \"" + digestAction
                                       + "\" as already executing, will try again later";
                LOG.warn(message);
            }
        }
    }

    // static member methods (alphabetic)

    /**
     * Converts the supplied String into the number of milliseconds to use as
     * the cushion.
     * 
     * @param requestedCushion
     *            the String representation of the cushion
     * 
     * @return the number of milliseconds to use as the cushion, or
     *         <code>null</code> if the supplied String can not be parsed.
     */
    private static Long parseCushion(final String requestedCushion) {
        if (null == requestedCushion) {
            return null;
        }
        final String[] parts = (requestedCushion.trim()).split("\\s+");
        if (2 != parts.length) {
            return null;
        }
        try {
            final TimeUnit timeUnit = TimeUnit.valueOf(parts[1].toUpperCase());
            return TimeUnit.MILLISECONDS.convert(Integer.parseInt(parts[0]),
                                                 timeUnit);
        } catch (IllegalArgumentException e) {
            return null;
        }
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}

}
