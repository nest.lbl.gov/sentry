package gov.lbl.nest.sentry;

import jakarta.ejb.Stateless;
import jakarta.ejb.TransactionAttribute;
import jakarta.ejb.TransactionAttributeType;

import gov.lbl.nest.common.configure.InitializingException;

/**
 * This class is soley to managed the execution of an {@link Invocation}
 * instance within a new transaction.
 * 
 * @author patton
 */
@Stateless
public class InvocationExecuter {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    /**
     * Executes the {@link Invocation} instance within a new transaction.
     * 
     * @param invocation
     *            the {@link Invocation} instance to exeute.
     * @param action
     *            the {@link RealizedAction} instance with which this method was
     *            invoked.
     * @param clazz
     *            the return type of the execution.
     * 
     * @return the result of executing this object.
     * 
     * @throws ExecutionException
     *             when the execution has not completed successfully.
     * @throws InitializingException
     */
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public <T> T execute(Invocation invocation,
                         RealizedAction action,
                         Class<T> clazz) throws ExecutionException, InitializingException {
        return invocation.execute(action,
                                  clazz);
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
