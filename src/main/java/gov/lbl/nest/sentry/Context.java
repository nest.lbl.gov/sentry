package gov.lbl.nest.sentry;

import java.io.File;

/**
 * This interface is the bases of any context used to create {@link Realization}
 * instances.
 * 
 * @author patton
 */
public interface Context {

    /**
     * Returns the directory containing this application's configurations.
     * 
     * @return the directory containing this application's configurations.
     */
    File getConfigDir();

    /**
     * Returns the instance of the class specified.
     * 
     * @param clazz
     *            the class of instance to return.
     * 
     * @return the instance of the class specified.
     */
    <T> T getInstance(Class<T> clazz);

    /**
     * Returns the instance of the class specified that has the supplied name.
     * 
     * @param name
     *            the name specifying the instance to return.
     * @param clazz
     *            the class of instance to return.
     * 
     * @return the instance of the class specified that has the supplied name.
     */
    <T> T getInstance(String name,
                      Class<T> clazz);
}
