package gov.lbl.nest.sentry;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import gov.lbl.nest.common.configure.InitializingException;
import gov.lbl.nest.common.suspension.Suspendable;
import gov.lbl.nest.sentry.config.Action;

/**
 * This class is a concrete realization of an {@link Action} instance.
 * 
 * @author patton
 */
public class Realization implements
                         Suspendable {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The {@link Action} instance used to create this object.
     */
    private final Action action;

    /**
     * The {@link Lock} instances used to serialize access to this object.
     */
    private final Lock lock = new ReentrantLock();

    /**
     * The realization of the action.
     */
    private final RealizedAction realizedAction;

    // constructors

    /**
     * Create an instance of this class.
     * 
     * @param action
     *            the {@link Action} to realize.
     * @param suspend
     *            True when execution of the created instance should be
     *            suspended at creation.
     * @param context
     *            the {@link Context} instance suppling the execution context to
     *            the created instance.
     * @param sentry
     *            the {@link Sentry} instance managing this object.
     * 
     * @throws InstantiationException
     *             if the action can not be realized.
     * @throws InvocationTargetException
     *             if the constructor of the action threw an exception.
     * @throws ClassNotFoundException
     *             if the class specified by the action can not be found.
     */
    public Realization(final Action action,
                       final Boolean suspend,
                       final Context context,
                       final Sentry sentry) throws InstantiationException,
                                            InvocationTargetException,
                                            ClassNotFoundException {
        this.action = action;

        final ClassLoader loader = getClass().getClassLoader();
        final String className = action.getActionClass();
        RealizedAction instance = null;
        final Class<?> clazz = loader.loadClass(className);
        try {
            final Constructor<?> constructor = clazz.getConstructor(Action.class,
                                                                    Boolean.class,
                                                                    Context.class,
                                                                    Sentry.class);
            instance = (RealizedAction) constructor.newInstance(action,
                                                                suspend,
                                                                context,
                                                                sentry);
        } catch (NoSuchMethodException e1) {
            // Do nothing, try next constructor.
        } catch (IllegalAccessException e) {
            // Do nothing, try next constructor.
        }
        if (null == instance) {
            try {
                final Constructor<?> constructor = clazz.getConstructor(Action.class,
                                                                        Boolean.class,
                                                                        Context.class);
                instance = (RealizedAction) constructor.newInstance(action,
                                                                    suspend,
                                                                    context);
            } catch (NoSuchMethodException e1) {
                // Do nothing, try next constructor.
            } catch (IllegalAccessException e) {
                // Do nothing, try next constructor.
            }
        }
        if (null == instance) {
            try {
                final Constructor<?> constructor = clazz.getConstructor(Action.class,
                                                                        Context.class);
                instance = (RealizedAction) constructor.newInstance(action,
                                                                    context);
            } catch (NoSuchMethodException e1) {
                // Do nothing, try next constructor.
            } catch (IllegalAccessException e) {
                // Do nothing, try next constructor.
            }
        }
        if (null == instance) {
            try {
                final Constructor<?> constructor = clazz.getConstructor(Action.class,
                                                                        Boolean.class);
                instance = (RealizedAction) constructor.newInstance(action,
                                                                    suspend);
            } catch (NoSuchMethodException e1) {
                // Do nothing, try next constructor.
            } catch (IllegalAccessException e) {
                // Do nothing, try next constructor.
            }
        }
        if (null == instance) {
            try {
                final Constructor<?> constructor = clazz.getConstructor(Action.class);
                instance = (RealizedAction) constructor.newInstance(action);
            } catch (NoSuchMethodException e1) {
                // Do nothing, try next constructor.
            } catch (IllegalAccessException e) {
                // Do nothing, try next constructor.
            }
        }
        if (null == instance) {
            try {
                final Constructor<?> constructor = clazz.getConstructor();
                instance = (RealizedAction) constructor.newInstance();
            } catch (NoSuchMethodException e1) {
                // Do nothing, try next constructor.
            } catch (IllegalAccessException e) {
                // Do nothing, try next constructor.
            }
        }
        if (null == instance) {
            throw new InstantiationException();
        }
        realizedAction = instance;
    }

    /**
     * Executes the {@link Invocation} instance using this object's action.
     * 
     * @param invocation
     *            the {@link Invocation} instance to execute.
     * @param executer
     *            the {@link InvocationExecuter} instance with which to execute
     *            the specified {@link Invocation} instance.
     * @param clazz
     *            the returned class of the invocation.
     * 
     * @return the result of executing the specified {@link Invocation}
     *         instance.
     * 
     * @throws TimeoutException
     *             when the lock for this object can not be acquired.
     * @throws ExecutionException
     *             when the execution has not completed successfully.
     * @throws InitializingException
     */
    public <T> T execute(Invocation invocation,
                         InvocationExecuter executer,
                         Class<T> clazz) throws TimeoutException,
                                         ExecutionException, InitializingException {
        if (lock.tryLock()) {
            try {
                return executer.execute(invocation,
                                        realizedAction,
                                        clazz);
            } finally {
                lock.unlock();
            }
            // Note: the "return" happens here after the "finally" block so
            // control will NOT continue outside this "if" block.
        }
        throw new TimeoutException();
    }

    /**
     * Returns the name of the action this object is realizing.
     * 
     * @return the name of the action this object is realizing.
     */
    public String getName() {
        return action.getName();
    }

    @Override
    public boolean isSuspended() throws InitializingException {
        if (realizedAction instanceof Suspendable
            && ((Suspendable) realizedAction).isSuspended()) {
            return true;
        }
        return false;
    }

    @Override
    public boolean setSuspended(boolean suspend) throws InitializingException {
        if (realizedAction instanceof Suspendable) {
            return ((Suspendable) realizedAction).setSuspended(suspend);
        }
        return false;
    }

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
