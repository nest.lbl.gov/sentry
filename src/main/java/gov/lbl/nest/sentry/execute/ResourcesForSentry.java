package gov.lbl.nest.sentry.execute;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.common.execution.DeployedFiles;
import gov.lbl.nest.common.execution.DeployedFiles.DeployedFile;
import gov.lbl.nest.jee.monitor.InstrumentationDB;
import gov.lbl.nest.jee.watching.WatcherDB;
import gov.lbl.nest.sentry.ImplementationString;
import gov.lbl.nest.sentry.MailSession;
import gov.lbl.nest.sentry.SentryDB;
import gov.lbl.nest.sentry.config.Configuration;
import jakarta.ejb.Lock;
import jakarta.ejb.LockType;
import jakarta.ejb.Singleton;
import jakarta.enterprise.inject.Produces;
import jakarta.mail.Authenticator;
import jakarta.mail.PasswordAuthentication;
import jakarta.mail.Session;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.xml.bind.JAXBException;

/**
 * This class provides resources to be injected into the sign-off components.
 * 
 * @author patton
 */
@Singleton
@Lock(value = LockType.READ)
public class ResourcesForSentry {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(ResourcesForSentry.class);

    /**
     * The name of the property containing the email credentials.
     */
    private static final String MAIL_CREDENTIALS_PROPERTY = "mail.credentials";

    /**
     * The name of the properties file contains the mail settings
     */
    private static final String MAIL_PROPERTIES = "mail.properties";

    /**
     * The name of the property containing the email credentials.
     */
    private static final String MAIL_USER_PROPERTY = "mail.user";

    // private static member data

    // private instance member data

    /**
     * Expose an entity manager using the resource producer pattern
     */
    @PersistenceContext(unitName = "sentry")
    private EntityManager sentryEntityManager;

    /**
     * The {@link Configuration} containing the configuration settings for Sentry.
     */
    private Configuration configuration;

    /**
     * The {@link Session} to be used by this application.
     */
    private Session session;

    /**
     * True if there has been an attempt to create the {@link Session} instance used
     * by this application.
     */
    private boolean sessionAttempted;

    /**
     * The version String containing this project's release.
     */
    private static String version;

    // constructors

    /**
     * Creates an instance of this class.
     */
    public ResourcesForSentry() {
        getDeployedVersion();
    }

    // instance member method (alphabetic)

    /**
     * Returns the configuration settings for Sentry.
     * 
     * @return the configuration settings for Sentry.
     */
    @Produces
    public Configuration getConfiguration() {
        if (null == configuration) {
            try {
                configuration = Configuration.load();
                LOG.info("Read configuration for \"" + "Sentry"
                         + "\" from \""
                         + configuration.getFile()
                         + "\"");
            } catch (JAXBException e1) {
                LOG.error("Could not parse configuration file for \"" + "Sentry"
                          + "\"");
                e1.printStackTrace();
            }

        }
        return configuration;
    }

    /**
     * Returns the {@link EntityManager} instance for Sentry database.
     * 
     * @return the {@link EntityManager} instance for Sentry database.
     */
    @Produces
    @SentryDB
    @InstrumentationDB
    @WatcherDB
    public EntityManager getSentryEntityManager() {
        return sentryEntityManager;
    }

    /**
     * Returns the {@link Session} to be used by the Sentry system.
     * 
     * @return the {@link Session} to be used by the Sentry system.
     */
    @Produces
    @MailSession
    public Session getSession() {
        if (!sessionAttempted) {
            Properties props = new Properties();
            try {
                final String propertiesFile = getConfiguration().getMailProperties();
                final File fileToUse;
                if (null == propertiesFile) {
                    fileToUse = new File(configuration.getDirectory(),
                                         MAIL_PROPERTIES);
                } else {
                    final File mailProperties = new File(propertiesFile);
                    if (mailProperties.isAbsolute()) {
                        fileToUse = mailProperties;
                    } else {
                        fileToUse = new File(configuration.getDirectory(),
                                             mailProperties.getPath());
                    }
                }
                if (!fileToUse.exists()) {
                    sessionAttempted = true;
                    return null;
                }
                props.load(new FileInputStream(fileToUse));
                final String mailUser = props.getProperty(MAIL_USER_PROPERTY);
                final String mailPassword = props.getProperty(MAIL_CREDENTIALS_PROPERTY);
                session = Session.getInstance(props,
                                              new Authenticator() {
                                                  @Override
                                                  protected PasswordAuthentication getPasswordAuthentication() {
                                                      return new PasswordAuthentication(mailUser,
                                                                                        mailPassword);
                                                  }
                                              });
                LOG.info("Read mail properties for \"Sentry\" from \"" + fileToUse.getPath()
                         + "\"");
            } catch (FileNotFoundException e) {
                session = Session.getInstance(new Properties());
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            final Set<String> names = props.stringPropertyNames();
            if (names.isEmpty()) {
                LOG.warn("No mail properties were specified for \"Sentry\" so will execute in standalone mode");
            } else {
                LOG.info("The following mail properties were specified for \"Sentry\"");
                for (String name : names) {
                    if (MAIL_CREDENTIALS_PROPERTY.equals(name)) {
                        LOG.info("  " + name
                                 + " = "
                                 + "<provided>");
                    } else {
                        LOG.info("  " + name
                                 + " = "
                                 + props.getProperty(name));
                    }
                }
            }
            sessionAttempted = true;
        }
        return session;
    }

    // static member methods (alphabetic)

    /**
     * Returns the version String containing this project's release.
     * 
     * @return the version String containing this project's release.
     */
    private static synchronized String getDeployedVersion() {
        if (null == version) {
            DeployedFile deployedFile = DeployedFiles.getDeployedWarFile();
            LOG.info("Using version \"" + deployedFile.version
                     + "\" of artifact \""
                     + deployedFile.project
                     + "\"");
            version = deployedFile.version;
        }
        return version;
    }

    /**
     * Returns the implementation version.
     * 
     * @return the implementation version.
     */
    @Produces
    @ImplementationString
    public String getImplementationString() {
        getDeployedVersion();
        return version;
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
