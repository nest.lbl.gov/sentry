package gov.lbl.nest.sentry.execute;

import jakarta.ejb.Schedule;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;

import gov.lbl.nest.common.configure.InitializingException;
import gov.lbl.nest.sentry.Sentry;

/**
 * This case executes the sentry activities periodically.
 * 
 * @author patton
 */
@Stateless
public class PeriodicExecutionForSentry {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The {@link Sentry} instances used by this object.
     */
    @Inject
    private Sentry sentry;

    // constructors

    // instance member method (alphabetic)

    /**
     * Watches for frequent tasks.
     */
    @Schedule(hour = "*",
              minute = "*/1",
              persistent = false)
    public void watchEveryMinute() {
        try {
            sentry.executeTask("minute");
        } catch (InitializingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Watches for semihourly tasks.
     */
    @Schedule(hour = "*",
              minute = "*/30",
              persistent = false)
    public void watchEveryHalfHour() {
        try {
            sentry.executeTask("semihourly");
        } catch (InitializingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Watches for frequent tasks.
     */
    @Schedule(dayOfWeek = "*",
              hour = "*/1",
              persistent = false)
    public void watchEveryHour() {
        try {
            sentry.executeTask("hourly");
        } catch (InitializingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Watches for nightly tasks (at 2am).
     */
    @Schedule(dayOfWeek = "*",
              hour = "2",
              persistent = false)
    public void watchEveryNight() {
        try {
            sentry.executeTask("nightly");
        } catch (InitializingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
