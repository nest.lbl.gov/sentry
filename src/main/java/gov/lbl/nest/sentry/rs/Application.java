package gov.lbl.nest.sentry.rs;

import java.net.URI;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class represents a user's entry point to all accessible resources in the
 * application.
 * 
 * @author patton
 */
@XmlRootElement(name = "application")
@XmlType(propOrder = { "uri",
                       "status",
                       "specification",
                       "implementation" })
public class Application {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The implementation version of this application.
     */
    private String implementation;

    /**
     * The specification version this application implements.
     */
    private String specification;

    /**
     * The current status of the application.
     */
    private Status status;

    /**
     * The URI of this object.
     */
    private URI uri;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected Application() {
    }

    /**
     * Creates an instance of this class.
     * 
     * @param uri
     *            the URI of this object.
     * @param status
     *            the current status of the application.
     * @param specification
     *            the specification version this application implements.
     * @param implementation
     *            the implementation version of this application.
     */
    Application(URI uri,
                Status status,
                String specification,
                String implementation) {
        setImplementation(implementation);
        setSpecification(specification);
        setStatus(status);
        setUri(uri);
    }

    // instance member method (alphabetic)

    /**
     * Returns the implementation version of this application.
     * 
     * @return the implementation version of this application.
     */
    @XmlElement
    protected String getImplementation() {
        return implementation;
    }

    /**
     * Returns the specification version this application implements.
     * 
     * @return the specification version this application implements.
     */
    @XmlElement
    protected String getSpecification() {
        return specification;
    }

    /**
     * Returns the current status of the application.
     * 
     * @return the current status of the application.
     */
    @XmlElement
    protected Status getStatus() {
        return status;
    }

    /**
     * Returns the URI of this object.
     * 
     * @return the URI of this object.
     */
    @XmlElement(name = "uri")
    protected URI getUri() {
        return uri;
    }

    /**
     * Sets the implementation version of this application.
     * 
     * @param implementation
     *            the implementation version of this application.
     */
    protected void setImplementation(String implementation) {
        this.implementation = implementation;
    }

    /**
     * Sets the specification version this application implements.
     * 
     * @param specification
     *            the specification version this application implements.
     */
    protected void setSpecification(String specification) {
        this.specification = specification;
    }

    /**
     * Sets the current status of the application.
     * 
     * @param status
     *            the current status of the application.
     */
    protected void setStatus(final Status status) {
        this.status = status;
    }

    /**
     * Sets the URI of this object.
     * 
     * @param uri
     *            the URI of this object.
     */
    protected void setUri(final URI uri) {
        this.uri = uri;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
