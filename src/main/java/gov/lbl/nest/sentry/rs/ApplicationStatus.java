package gov.lbl.nest.sentry.rs;

import java.net.URI;
import java.util.List;

import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlRootElement;

/**
 * This class is used to communicate, via the RESTful interface, the current
 * status of the the application.
 * 
 * @author patton
 */
@XmlRootElement(name = "application_status")
public class ApplicationStatus extends
                               Status {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The statuses of all of the actions that make up the application.
     */
    private List<ActionStatus> actions;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected ApplicationStatus() {
    }

    /**
     * Creates an instance of this class.
     * 
     * @param uri
     *            the URI of this object.
     * @param name
     *            the name of the he application.
     * @param execution
     *            the current execution state of the application.
     */
    public ApplicationStatus(URI uri,
                             final String name,
                             final Execution execution) {
        super(uri,
              name,
              execution,
              true);
    }

    /**
     * Creates an instance of this class.
     * 
     * @param uri
     *            the URI of this object.
     * @param name
     *            the name of the he application.
     * @param execution
     *            the current execution state of the application.
     * @param actions
     *            the statuses of all of the actions that make up the
     *            application.
     */
    public ApplicationStatus(final URI uri,
                             final String name,
                             final Execution execution,
                             final List<ActionStatus> actions) {
        super(uri,
              name,
              execution,
              null);
        this.actions = actions;
    }

    // instance member method (alphabetic)

    /**
     * Returns the statuses of all of the actions that make up the application.
     * 
     * @return the statuses of all of the actions that make up the application.
     */
    @XmlElement(name = "action")
    public List<ActionStatus> getactions() {
        return actions;
    }

    /**
     * Sets the statuses of all of the actions that make up the application.
     * 
     * @param actions
     *            the statuses of all of the actions that make up the
     *            application.
     */
    protected void setactions(final List<ActionStatus> actions) {
        this.actions = actions;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
