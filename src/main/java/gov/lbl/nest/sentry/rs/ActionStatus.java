package gov.lbl.nest.sentry.rs;

import java.net.URI;

import jakarta.xml.bind.annotation.XmlRootElement;

/**
 * This class is used to communicate, via the RESTful interface, the current
 * status of the an action.
 * 
 * @author patton
 */
@XmlRootElement(name = "action_status")
public class ActionStatus extends
                          Status {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected ActionStatus() {
    }

    /**
     * Creates an instance of this class.
     * 
     * @param uri
     *            the URI of this object.
     * @param name
     *            the name of the he application.
     * @param execution
     *            the current execution state of the application.
     */
    public ActionStatus(URI uri,
                        final String name,
                        final Execution execution) {
        super(uri,
              name,
              execution,
              null);
    }

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
