package gov.lbl.nest.sentry.rs;

/**
 * The class contains the media types used by the RESTful interface.
 * 
 * @author patton
 * 
 */
public class SentryType {

    /**
     * The media type for the XML representation of an {@link Application}
     * instance.
     */
    public static final String APPLICATION_XML = "application/gov.lbl.nest.sentry.rs.Application+xml";

    /**
     * The media type for the JSON representation of an {@link Application}
     * instance.
     */
    public static final String APPLICATION_JSON = "application/gov.lbl.nest.sentry.rs.Application+json";

}
