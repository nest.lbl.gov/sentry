package gov.lbl.nest.sentry.rs;

import jakarta.ws.rs.ApplicationPath;
import jakarta.ws.rs.core.Application;

/**
 * This class provides the location of the RESTful access to the Sentry
 * application.
 * 
 * @author patton
 */
@ApplicationPath("local")
public class SentryApplication extends
                               Application {
    // Does nothing, just a label.
}
