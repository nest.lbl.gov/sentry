package gov.lbl.nest.sentry.rs;

import jakarta.mail.MessagingException;
import jakarta.mail.Part;

public class UtilitiesForSentry {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    /**
     * Returns the name of the form-data parameter, otherwise returns
     * <code>null</code>. See RFC2388 for details.
     * 
     * @param part
     *            the part whose parameter name is required.
     * 
     * @return the name of the form-data parameter, otherwise returns
     *         <code>null</code>.
     * 
     * @throws MessagingException
     *             when the disposition of the part can not be established.
     */
    public static String getName(Part part) throws MessagingException {
        final String disposition = part.getDisposition();
        if (null != disposition && disposition.equals("form-data")) {
            final String[] v = part.getHeader("Content-Disposition");
            for (String v1 : v) {
                final String[] s = v1.split(";");
                for (String s1 : s) {
                    final String t = s1.trim();
                    if (t.startsWith("name=")) {
                        return t.substring(6,
                                           t.length() - 1);
                    }
                }
            }
        }
        return null;
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
