package gov.lbl.nest.sentry.rs;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import gov.lbl.nest.common.configure.InitializingException;
import gov.lbl.nest.common.suspension.Suspendable;
import gov.lbl.nest.sentry.ImplementationString;
import gov.lbl.nest.sentry.Sentry;
import gov.lbl.nest.sentry.SentrySuspension;
import gov.lbl.nest.sentry.config.ApplicationSuspension;
import gov.lbl.nest.sentry.config.Configuration;
import gov.lbl.nest.sentry.rs.Status.Execution;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.ResponseBuilder;
import jakarta.ws.rs.core.UriInfo;

/**
 * The class provides a RESTful interface to access Sentry application status.
 * 
 * @author patton
 * 
 * @see CommandSentry
 */
@Path("report")
@Stateless
public class ReportSentry {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The implementation version.
     */
    @Inject
    @ImplementationString
    private String implementation;

    /**
     * The {@link Sentry} instance this instance is using.
     */
    @Inject
    private Sentry sentry;

    /**
     * The {@link ApplicationSuspension} instance containing the configuration
     * settings for Sentry.
     */
    @Inject
    @SentrySuspension
    private Suspendable suspended;

    // constructors

    // instance member method (alphabetic)

    /**
     * Returns the representation of this application.
     * 
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * 
     * @return the representation of this application.
     * 
     * @throws InitializingException
     *             when the application is still initializing.
     */
    @GET
    @Path("")
    @Produces({ SentryType.APPLICATION_XML,
                SentryType.APPLICATION_JSON,
                MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Application getApplication(@Context UriInfo uriInfo) throws InitializingException {
        final URI baseUri = uriInfo.getBaseUri();
        final URI reportUri = baseUri.resolve("report/");

        return new Application(reportUri,
                               createApplicationStatus(uriInfo,
                                                       sentry,
                                                       true),
                               Sentry.SPECIFICATION,
                               implementation);
    }

    /**
     * Returns the XML representation of the detailed status of the application.
     * 
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * 
     * @return the {@link Response} generated for this request. It contains the XML
     *         representation of the detailed status of the application.
     * 
     * @throws InitializingException
     *             when the application is still initializing.
     */
    @GET
    @Path("application/status")
    @Produces({ MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response getApplicationStatus(@Context UriInfo uriInfo) throws InitializingException {
        return createApplicationStatusReponse(uriInfo,
                                              sentry,
                                              false);
    }

    // static member methods (alphabetic)

    /**
     * Creates a new {@link ApplicationStatus} instance to reflect the current state
     * of the application.
     * 
     * @param uriInfo
     * @param spade
     *            The {@link Spade} instance whose {@link ApplicationStatus}
     *            instance should be created.
     * @param summary
     *            true if only a summary status should be created.
     * 
     * @return the new {@link ApplicationStatus} instance to reflect the current
     *         state of the application.
     * 
     * @throws InitializingException
     *             when the application is still initializing.
     */
    private static ApplicationStatus createApplicationStatus(UriInfo uriInfo,
                                                             Sentry sentry,
                                                             boolean summary) throws InitializingException {
        final URI baseUri = uriInfo.getBaseUri();
        final URI reportUri = baseUri.resolve("report/");

        if (summary) {
            return new ApplicationStatus(reportUri.resolve("application/status"),
                                         "sentry", // sentry.getIdentity(),
                                         getApplicationExecution(sentry));
        }
        final List<ActionStatus> actions = new ArrayList<ActionStatus>();
        final ApplicationStatus status = new ApplicationStatus(reportUri.resolve("application/status"),
                                                               "sentry", // sentry.getIdentity(),
                                                               getApplicationExecution(sentry),
                                                               actions);
        return status;
    }

    static Response createApplicationStatusReponse(UriInfo uriInfo,
                                                   Sentry sentry,
                                                   boolean summary) throws InitializingException {
        final ApplicationStatus status = createApplicationStatus(uriInfo,
                                                                 sentry,
                                                                 summary);
        ResponseBuilder builder = Response.status(Response.Status.OK);
        builder = builder.entity(status);
        return builder.build();
    }

    /**
     * Returns the {@link Execution} instance appropriate for the current state of
     * the application.
     * 
     * @param sentry
     *            The {@link Sentry} instance whose {@link Execution} instance
     *            should be returned.
     * 
     * @return the {@link Execution} instance appropriate for the current state of
     *         the application.
     * 
     * @throws InitializingException
     *             when the application is still initializing.
     */
    private static Execution getApplicationExecution(final Sentry sentry) throws InitializingException {
        final boolean applicationSuspended = sentry.isSuspended();
        if (applicationSuspended) {
            return Execution.SUSPENDED;
        }
        return Execution.RUNNING;
    }

    /**
     * Returns the XML representation of the configuration file used by the
     * application.
     * 
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * 
     * @return the {@link Response} generated for this request. It contains the XML
     *         representation of the detailed status of the application.
     */
    @GET
    @Path("application/configuration")
    @Produces({ MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response getConfiguration(@Context UriInfo uriInfo) {
        ResponseBuilder builder;
        try {
            final Configuration configuration = sentry.getConfiguration();
            builder = Response.status(Response.Status.OK);
            builder = builder.entity(configuration);
        } catch (Exception e) {
            e.printStackTrace();
            builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
        }
        return builder.build();
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
