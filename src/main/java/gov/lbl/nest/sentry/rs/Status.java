package gov.lbl.nest.sentry.rs;

import java.net.URI;
import java.util.Date;

import jakarta.xml.bind.annotation.XmlAttribute;
import jakarta.xml.bind.annotation.XmlElement;
import jakarta.xml.bind.annotation.XmlEnum;
import jakarta.xml.bind.annotation.XmlType;

/**
 * This class is used to communicate, via the RESTful interface, the current
 * status of the component of the application.
 * 
 * @author patton
 */
@XmlType(propOrder = { "uri",
                       "name",
                       "timestamp",
                       "execution" })
public class Status {

    /**
     * This enumerates the possible execution states of a component of the
     * application.
     * 
     * @author patton
     */
    @XmlEnum(String.class)
    public enum Execution {
                           /**
                            * In this state the component is running and none of
                            * its constituent components have been suspended.
                            */
                           RUNNING,

                           /**
                            * In this state the component is running and one of
                            * more of its constituent components have been
                            * suspended.
                            */
                           PARTIALLY_RUNNING,

                           /**
                            * In this state the component has been suspended but
                            * one of more of its constituent components are
                            * running.
                            */
                           PARTIALLY_SUSPENDED,

                           /**
                            * In this state the component has been suspended and
                            * none of its constituent components are running.
                            */
                           SUSPENDED
    }

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    /**
     * The current execution state of the component of the application.
     */
    private Execution execution;

    /**
     * The name of this instance of the component of the application.
     */
    private String name;

    /**
     * <code>true</code> if this status is not the complete one.
     */
    private Boolean summary;

    /**
     * The time this status was created.
     */
    private Date timestamp;

    /**
     * The URI of this object.
     */
    private URI uri;

    // constructors

    /**
     * Creates an instance of this class.
     */
    protected Status() {
    }

    /**
     * Creates an instance of this class.
     * 
     * @param uri
     *            the URI of this object.
     * @param name
     *            the name of the component of the component of the application
     * @param execution
     *            the current execution state of the component of the
     *            application.
     * @param summary
     *            <code>true</code> if this status is not the complete one.
     */
    public Status(final URI uri,
                  final String name,
                  final Execution execution,
                  final Boolean summary) {
        setExecution(execution);
        setName(name);
        setSummary(summary);
        if (null == summary || summary == Boolean.FALSE) {
            setTimestamp(new Date());
        }
        setUri(uri);
    }

    // instance member method (alphabetic)

    /**
     * Returns the current execution state of the component of the application.
     * 
     * @return the current execution state of the component of the application.
     */
    @XmlElement
    protected Execution getExecution() {
        return execution;
    }

    /**
     * Returns the name of this instance of the component of the application.
     * 
     * @return the name of this instance of the component of the application.
     */
    @XmlElement(name = "identity")
    protected String getName() {
        return name;
    }

    /**
     * Returns <code>true</code> if this status is not the complete one.
     * 
     * @return <code>true</code> if this status is not the complete one.
     */
    @XmlAttribute
    protected Boolean getSummary() {
        return summary;
    }

    /**
     * Returns the time this status was created.
     * 
     * @return the time this status was created.
     */
    @XmlElement(name = "timestamp")
    protected Date getTimestamp() {
        return timestamp;
    }

    /**
     * Returns the URI of this object.
     * 
     * @return the URI of this object.
     */
    @XmlElement
    protected URI getUri() {
        return uri;
    }

    /**
     * Sets the current execution state of the component of the application.
     * 
     * @param execution
     *            the current execution state of the component of the
     *            application.
     */
    public void setExecution(final Execution execution) {
        this.execution = execution;
    }

    /**
     * Set the name of this instance of the component of the application.
     * 
     * @param name
     *            the name of this instance of the component of the application.
     */
    protected void setName(String name) {
        this.name = name;
    }

    /**
     * Sets whether this status is not the complete one or not.
     * 
     * @param summary
     *            <code>true</code> if this status is not the complete one.
     */
    protected void setSummary(Boolean summary) {
        this.summary = summary;
    }

    /**
     * Sets t the time this status was created.
     * 
     * @param timestamp
     *            t the time this status was created.
     */
    protected void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * Sets the URI of this object.
     * 
     * @param uri
     *            the URI of this object.
     */
    protected void setUri(URI uri) {
        this.uri = uri;
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
