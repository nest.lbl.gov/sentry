package gov.lbl.nest.sentry.rs;

import java.util.Collection;
import java.util.concurrent.TimeoutException;

import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.ResponseBuilder;
import jakarta.ws.rs.core.UriInfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import gov.lbl.nest.sentry.ExecutionException;
import gov.lbl.nest.sentry.Sentry;

/**
 * The class provides a RESTful interface to execute Sentry application
 * commands.
 * 
 * @author patton
 * 
 * @see ReportSentry
 */
@Path("command")
@Stateless
public class CommandSentry {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    /**
     * The {@link Logger} used by this class.
     */
    private static final Logger LOG = LoggerFactory.getLogger(CommandSentry.class);

    // private static member data

    // private instance member data

    /**
     * The {@link Sentry} instance this instance is using.
     */
    @Inject
    private Sentry sentry;

    // instance member method (alphabetic)

    /**
     * Executes all of Sentry's actions.
     * 
     * @return the {@link Response} generated for this request.
     */
    @Path("execute")
    @POST
    public Response execute() {
        try {
            final Collection<String> actions = sentry.getActions();
            final String plural;
            if (1 == actions.size()) {
                plural = "";
            } else {
                plural = "s";
            }
            final StringBuilder sb = new StringBuilder("Manually executing the following action"
                                                       + plural
                                                       + ": ");
            String conjunction = "";
            for (String action : actions) {
                sb.append(conjunction + "\""
                          + action
                          + "\"");
                conjunction = ", ";
            }
            LOG.info(sb.toString());
            boolean failed = false;
            for (String action : actions) {
                try {
                    // By executing each action explicitly via the Sentry
                    // interface they execute in separate transactions.
                    sentry.execute(action);
                    LOG.info("Successfully completed manual execution of sentry actions");
                } catch (TimeoutException e) {
                    LOG.warn("Skipped \"" + action
                             + "\" as already executing");
                } catch (Exception e) {
                    e.printStackTrace();
                    failed = true;
                }
            }
            if (failed) {
                LOG.error("Failed during manual execution of sentry actions");
                ResponseBuilder builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
                return builder.build();
            }
            ResponseBuilder builder = Response.status(Response.Status.ACCEPTED);
            return builder.build();
        } catch (Exception e) {
            LOG.error("Failed during manual execution of sentry actions");
            e.printStackTrace();
            ResponseBuilder builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
            return builder.build();
        }
    }

    /**
     * Executes the actions that belong to the specified task.
     * 
     * @param task
     *            the task whose actions should be executed.
     * 
     * @return the {@link Response} generated for this request.
     */
    @Path("task/{task}/execute")
    @POST
    public Response executeTask(@PathParam("task") String task) {
        try {
            final Collection<String> actions = sentry.getActions(task);
            if (0 == actions.size()) {
                LOG.warn("Task \"" + task
                         + "\" contains no actions, so will not be manually executed");
                ResponseBuilder builder = Response.status(Response.Status.ACCEPTED);
                return builder.build();
            }
            final String plural;
            if (1 == actions.size()) {
                plural = "";
            } else {
                plural = "s";
            }
            final StringBuilder sb = new StringBuilder("Manually executing the \""
                                                       + task
                                                       + "\" task which is made up of the following action"
                                                       + plural
                                                       + ": ");
            String conjunction = "";
            for (String action : actions) {
                sb.append(conjunction + "\""
                          + action
                          + "\"");
                conjunction = ", ";
            }
            LOG.info(sb.toString());
            boolean failed = false;
            for (String action : actions) {
                try {
                    // By executing each action explicitly via the Sentry
                    // interface they execute in separate transactions.
                    sentry.execute(action);
                } catch (TimeoutException e) {
                    LOG.warn("Skipped \"" + action
                             + "\" as already executing");
                } catch (ExecutionException e) {
                    failed = true;
                }
            }
            if (failed) {
                LOG.error("Failed during manual execution of the \"" + task
                          + "\" task");
                ResponseBuilder builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
                return builder.build();
            }
            LOG.info("Successfully completed manual execution of the \"" + task
                     + "\" task");
            ResponseBuilder builder = Response.status(Response.Status.ACCEPTED);
            return builder.build();
        } catch (Exception e) {
            LOG.error("Failed during manual execution of the \"" + task
                      + "\" task");
            e.printStackTrace();
            ResponseBuilder builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
            return builder.build();
        }
    }

    /**
     * Executes a action
     * 
     * @param action
     *            the action that should be executed.
     * 
     * @return the {@link Response} generated for this request.
     */
    @Path("action/{action}/execute")
    @POST
    public Response executeAction(@PathParam("action") String action) {
        try {
            try {
                // By executing each action explicitly via the Sentry
                // interface they execute in separate transactions.
                sentry.execute(action);
            } catch (TimeoutException e) {
                LOG.warn("Skipped manual execution of the \"" + action
                         + "\" action as it was already executing");
            }
            ResponseBuilder builder = Response.status(Response.Status.ACCEPTED);
            return builder.build();
        } catch (Exception e) {
            LOG.error("Failed during manual execution of the \"" + action
                      + "\" action");
            e.printStackTrace();
            ResponseBuilder builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
            return builder.build();
        }
    }

    /**
     * Attempts to resume the specified action if it is suspended.
     * 
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param action
     *            the name of the action to resume.
     * 
     * @return the {@link Response} generated for this request.
     */
    @POST
    @Path("action/{action}/resume")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_XML)
    public Response resumeAction(@Context UriInfo uriInfo,
                                 @PathParam("action") String action) {
        try {
            sentry.setSuspendedAction(action,
                                      false);
            return ReportSentry.createApplicationStatusReponse(uriInfo,
                                                               sentry,
                                                               false);
        } catch (Exception e) {
            LOG.error("Failed when trying to resume \"" + action
                      + "\"");
            e.printStackTrace();
            ResponseBuilder builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
            return builder.build();
        }
    }

    /**
     * Attempts to resume the execution of all activities of the application.
     * 
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * 
     * @return the {@link Response} generated for this request. It contains the
     *         XML representation of the detailed status of the application.
     */
    @POST
    @Path("application/resume")
    @Produces({ MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response resumeApplication(@Context UriInfo uriInfo) {
        try {
            sentry.setSuspended(false);
            return ReportSentry.createApplicationStatusReponse(uriInfo,
                                                               sentry,
                                                               false);
        } catch (Exception e) {
            LOG.error("Failed when trying to resume the application");
            e.printStackTrace();
            ResponseBuilder builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
            return builder.build();
        }
    }

    /**
     * Attempts to resume the specified task if it is suspended.
     * 
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param task
     *            the name of the action to resume.
     * 
     * @return the {@link Response} generated for this request.
     */
    @POST
    @Path("task/{task}/resume")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_XML)
    public Response resumeTask(@Context UriInfo uriInfo,
                               @PathParam("task") String task) {
        try {
            sentry.setSuspended(task,
                                false);
            return ReportSentry.createApplicationStatusReponse(uriInfo,
                                                               sentry,
                                                               false);
        } catch (Exception e) {
            LOG.error("Failed when trying to resume \"" + task
                      + "\"");
            e.printStackTrace();
            ResponseBuilder builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
            return builder.build();
        }
    }

    /**
     * Attempts to suspend the specified action if it is running.
     * 
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param action
     *            the name of the action to suspend.
     * 
     * @return the {@link Response} generated for this request.
     */
    @POST
    @Path("action/{action}/suspend")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_XML)
    public Response suspendAction(@Context UriInfo uriInfo,
                                  @PathParam("action") String action) {
        try {
            sentry.setSuspendedAction(action,
                                      true);
            return ReportSentry.createApplicationStatusReponse(uriInfo,
                                                               sentry,
                                                               false);
        } catch (Exception e) {
            LOG.error("Failed when trying to suspend \"" + action
                      + "\"");
            e.printStackTrace();
            ResponseBuilder builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
            return builder.build();
        }
    }

    /**
     * Attempts to suspend the execution of all activities of the application.
     * 
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * 
     * @return the {@link Response} generated for this request. It contains the
     *         XML representation of the detailed status of the application.
     */
    @POST
    @Path("application/suspend")
    @Produces({ MediaType.APPLICATION_XML,
                MediaType.APPLICATION_JSON })
    public Response suspendApplication(@Context UriInfo uriInfo) {
        try {
            sentry.setSuspended(true);
            return ReportSentry.createApplicationStatusReponse(uriInfo,
                                                               sentry,
                                                               false);
        } catch (Exception e) {
            LOG.error("Failed when trying to suspend the application");
            e.printStackTrace();
            ResponseBuilder builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
            return builder.build();
        }
    }

    /**
     * Attempts to suspend the specified task if it is running.
     * 
     * @param uriInfo
     *            the {@link UriInfo} of the request.
     * @param task
     *            the name of the action to suspend.
     * 
     * @return the {@link Response} generated for this request.
     */
    @POST
    @Path("task/{task}/suspend")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_XML)
    public Response suspendTask(@Context UriInfo uriInfo,
                                @PathParam("task") String task) {
        try {
            sentry.setSuspended(task,
                                true);
            return ReportSentry.createApplicationStatusReponse(uriInfo,
                                                               sentry,
                                                               false);
        } catch (Exception e) {
            LOG.error("Failed when trying to suspend \"" + task
                      + "\"");
            e.printStackTrace();
            ResponseBuilder builder = Response.status(Response.Status.INTERNAL_SERVER_ERROR);
            return builder.build();
        }
    }

    // static member methods (alphabetic)

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
