package gov.lbl.nest.sentry;

import gov.lbl.nest.common.configure.InitializingException;

/**
 * Any class that is realized by Sentry must inplement this interface.
 * 
 * @author patton
 */
public interface RealizedAction {

    /**
     * The method to be executed by this action.
     * 
     * @throws ExecutionException
     *             when the execution has not completed successfully.
     * @throws InitializingException
     *             when the application is still initializing.
     */
    void execute() throws ExecutionException,
                   InitializingException;
}
