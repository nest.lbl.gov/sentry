package gov.lbl.nest.sentry;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * This class test that a {@link SentryImpl} implementation fulfills its
 * requirements.
 * 
 * @author patton
 */
@DisplayName("Placeholder")
public class TestSentry {

    // public static final member data

    // protected static final member data

    // static final member data

    // private static final member data

    // private static member data

    // private instance member data

    // constructors

    // instance member method (alphabetic)

    // static member methods (alphabetic)

    /**
     * Need to write tests!
     */
    @Test
    @DisplayName("Non-Test")
    public void testNull() {
        // Does nothing, just a placeholder.
    }

    // Description of this object.
    // @Override
    // public String toString() {}

    // public static void main(String args[]) {}
}
